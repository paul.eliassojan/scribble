# frozen_string_literal: true

class CreateSchedulers < ActiveRecord::Migration[6.1]
  def change
    create_table :schedulers, id: :uuid do |t|
      t.datetime :scheduling_date_time, null: false
      t.string :status, null: false
      t.references :article, null: false, foreign_key: true, type: :uuid
      t.timestamps
    end
  end
end
