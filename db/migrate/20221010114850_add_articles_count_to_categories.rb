# frozen_string_literal: true

class AddArticlesCountToCategories < ActiveRecord::Migration[6.1]
  def self.up
    add_column :categories, :articles_count, :integer, null: false, default: 0
  end

  def self.down
    remove_column :categories, :articles_count
  end
end
