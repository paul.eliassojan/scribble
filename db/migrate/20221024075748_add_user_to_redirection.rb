# frozen_string_literal: true

class AddUserToRedirection < ActiveRecord::Migration[6.1]
  def change
    add_reference :redirections, :user, type: :uuid, foreign_key: true
  end
end
