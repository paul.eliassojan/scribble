# frozen_string_literal: true

class AddUserToCategories < ActiveRecord::Migration[6.1]
  def change
    Category.reset_column_information
    add_reference :categories, :user, type: :uuid, foreign_key: true
  end
end
