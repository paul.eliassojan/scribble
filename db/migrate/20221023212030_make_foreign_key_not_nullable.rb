# frozen_string_literal: true

class MakeForeignKeyNotNullable < ActiveRecord::Migration[6.1]
  def change
    change_column_null :users, :organization_id, false
    change_column_null :categories, :user_id, false
  end
end
