# frozen_string_literal: true

class AddRestoreArticleTimeToArticlesTable < ActiveRecord::Migration[6.1]
  def change
    add_column :articles, :restore_time, :string
  end
end
