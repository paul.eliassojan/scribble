# frozen_string_literal: true

class SeedUserIdToExistingCategories < ActiveRecord::Migration[6.1]
  def change
    Category.find_each do |category|
      category.update(user_id: User.first.id)
    end
  end
end
