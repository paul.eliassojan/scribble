# frozen_string_literal: true

class SeedOrganizationIdToUser < ActiveRecord::Migration[6.1]
  def change
    User.find_each do |user|
      user.update(organization_id: Organization.first.id)
    end
  end
end
