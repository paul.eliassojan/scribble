# frozen_string_literal: true

class CreateArticles < ActiveRecord::Migration[6.1]
  def change
    create_table :articles do |t|
      t.text :title, null: false
      t.string :status, default: "draft", null: false
      t.string :slug, null: false
      t.timestamps
    end
  end
end
