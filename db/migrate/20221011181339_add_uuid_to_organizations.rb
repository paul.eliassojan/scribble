# frozen_string_literal: true

require "webdack/uuid_migration/helpers"

class AddUuidToOrganizations < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do

        enable_extension "pgcrypto"

        primary_key_to_uuid :organizations

        columns_to_uuid :organizations
      end

      dir.down do
        raise ActiveRecord::IrreversibleMigration
      end
    end
  end
end
