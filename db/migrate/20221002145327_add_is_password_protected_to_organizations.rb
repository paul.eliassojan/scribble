# frozen_string_literal: true

class AddIsPasswordProtectedToOrganizations < ActiveRecord::Migration[6.1]
  def change
    add_column :organizations, :password_protected, :boolean, default: false
  end
end
