# frozen_string_literal: true

class AddUniqueTitleForCategory < ActiveRecord::Migration[6.1]
  def change
    add_index :categories, :title, unique: true
  end
end
