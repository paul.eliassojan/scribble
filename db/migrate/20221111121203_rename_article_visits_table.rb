# frozen_string_literal: true

class RenameArticleVisitsTable < ActiveRecord::Migration[6.1]
  def change
    rename_table :article_visits, :visits
  end
end
