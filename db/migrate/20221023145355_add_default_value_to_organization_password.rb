# frozen_string_literal: true

class AddDefaultValueToOrganizationPassword < ActiveRecord::Migration[6.1]
  def change
    change_column_default :organizations, :password_digest, ""
  end
end
