# frozen_string_literal: true

class AddIsScheduledToArticles < ActiveRecord::Migration[6.1]
  def change
    add_column :articles, :is_scheduled, :boolean, default: false
  end
end
