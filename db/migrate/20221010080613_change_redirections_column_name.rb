# frozen_string_literal: true

class ChangeRedirectionsColumnName < ActiveRecord::Migration[6.1]
  def change
    rename_column :redirections, :to, :to_path
    rename_column :redirections, :from, :from_path
  end
end
