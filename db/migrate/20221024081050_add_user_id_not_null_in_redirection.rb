# frozen_string_literal: true

class AddUserIdNotNullInRedirection < ActiveRecord::Migration[6.1]
  def change
    change_column_null :redirections, :user_id, false
  end
end
