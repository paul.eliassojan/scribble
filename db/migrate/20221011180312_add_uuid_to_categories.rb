# frozen_string_literal: true

require "webdack/uuid_migration/helpers"

class AddUuidToCategories < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        enable_extension "pgcrypto"

        primary_key_and_all_references_to_uuid :categories
      end

      dir.down do
        raise ActiveRecord::IrreversibleMigration
      end
    end
  end
end
