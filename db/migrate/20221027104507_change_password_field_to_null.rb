# frozen_string_literal: true

class ChangePasswordFieldToNull < ActiveRecord::Migration[6.1]
  def change
    change_column_null :organizations, :password_digest, null: true
  end
end
