# frozen_string_literal: true

class AddLastPublishedDateToArticles < ActiveRecord::Migration[6.1]
  def change
    add_column :articles, :last_published_at, :datetime
  end
end
