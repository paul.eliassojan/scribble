# frozen_string_literal: true

Rails.application.routes.draw do
  defaults format: :json do
    namespace :api do
      resources :articles, except: %i[new edit] do
        resources :versions, except: %i[new edit create], controller: "article_versions"
        resources :schedulers, except: %i[new edit], controller: "article_schedulers"
      end
      resources :analytics, only: %i[index show], controller: "article_analytics"
      resources :categories, except: %i[new edit show]
      resources :redirections, except: %i[new edit show]
      resource :organization, only: %i[show update]
      resource :session, only: :create
      namespace :public do
        resources :categories, only: %i[index show], param: :slug
        resources :articles, only: %i[index show], param: :slug
      end
    end
  end

  post "api/login", to: "api/sessions#create"
  root "home#index"
  get "*path", to: "home#index", via: :all
end
