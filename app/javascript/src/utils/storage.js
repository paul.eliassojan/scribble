const setOrganizationNameToLocalStorage = organizationName => {
  localStorage.setItem("organizationName", organizationName);
};

const getFromLocalStorage = key => localStorage.getItem(key);

export { getFromLocalStorage, setOrganizationNameToLocalStorage };
