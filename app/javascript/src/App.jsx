import React, { useState, useEffect } from "react";

import { PageLoader } from "neetoui";

import { setAuthHeaders, registerIntercepts } from "apis/axios";
import Main from "components/Main";
import "lib/dayjs"; // eslint-disable-line

const App = ({ isLoggedIn }) => {
  const [loading, setLoading] = useState(true);

  const registerInterceptsAndSetAuthHeaders = async () => {
    Promise.all([registerIntercepts(), setAuthHeaders(setLoading)]);
  };

  useEffect(() => {
    registerInterceptsAndSetAuthHeaders();
  }, []);

  if (loading) {
    return (
      <div className="h-screen">
        <PageLoader />
      </div>
    );
  }

  return <Main isLoggedIn={isLoggedIn} />;
};

export default App;
