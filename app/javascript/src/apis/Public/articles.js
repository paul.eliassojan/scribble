import axios from "axios";

const list = searchQuery =>
  axios.get(`/api/public/articles/?search=${searchQuery}`);

const show = (articleSlug, categorySlug) =>
  axios.get(`/api/public/articles/${articleSlug}?category=${categorySlug}`);

const publicArticlesApi = { list, show };

export default publicArticlesApi;
