import axios from "axios";

const list = () => axios.get("/api/public/categories");

const articlesList = slug => axios.get(`/api/public/categories/${slug}`);

const publicCategoriesApi = { list, articlesList };

export default publicCategoriesApi;
