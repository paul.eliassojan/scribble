import axios from "axios";

const login = payload => axios.post("/api/login", payload);

const authApi = { login };

export default authApi;
