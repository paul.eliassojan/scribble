import axios from "axios";

const list = () => axios.get("/api/categories");

const create = payload => axios.post("/api/categories", payload);

const update = ({ id, payload, quiet = false }) => {
  const path = quiet ? `/api/categories/${id}?quiet` : `/api/categories/${id}`;
  return axios.put(path, payload);
};
const destroy = (currentCategory, newCategory) =>
  axios.delete(`/api/categories/${currentCategory}?category_id=${newCategory}`);

const categoriesApi = { list, create, update, destroy };

export default categoriesApi;
