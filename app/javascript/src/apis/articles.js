import axios from "axios";

const list = (articleStatus, category, searchQuery, page, limit) =>
  axios.get(
    `/api/articles/?status=${articleStatus}&category=${category}&search=${searchQuery}&page=${page}&limit=${limit}`
  );

const create = ({ payload }) =>
  axios.post("/api/articles", { article: payload });

const createSchedule = ({ articleData, schedulerData }) =>
  axios.post("/api/articles?quiet", {
    article: articleData,
    scheduler: schedulerData,
  });

const destroy = id => axios.delete(`/api/articles/${id}`);

const show = id => axios.get(`/api/articles/${id}`);

const update = (payload, id) =>
  axios.put(`/api/articles/${id}`, { article: payload });

const articlesApi = { list, create, destroy, show, update, createSchedule };

export default articlesApi;
