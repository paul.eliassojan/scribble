import axios from "axios";

const list = (page, limit) =>
  axios.get(`/api/analytics?page=${page}&limit=${limit}`);

const show = articleId => axios.get(`/api/analytics/${articleId}`);

const articleAnalyticsApi = { list, show };

export default articleAnalyticsApi;
