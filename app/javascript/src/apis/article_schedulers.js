import axios from "axios";

const list = articleId => axios.get(`/api/articles/${articleId}/schedulers`);

const create = (articleId, payload) =>
  axios.post(`/api/articles/${articleId}/schedulers`, { scheduler: payload });

const destroy = (articleId, scheduledId) =>
  axios.delete(`/api/articles/${articleId}/schedulers/${scheduledId}`);

const articleSchedulersApi = { list, create, destroy };

export default articleSchedulersApi;
