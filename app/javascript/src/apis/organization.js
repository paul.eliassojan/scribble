import axios from "axios";

const show = () => axios.get("/api/organization");

const update = payload =>
  axios.put(`/api/organization`, { organization: payload });

const organizationApi = { show, update };

export default organizationApi;
