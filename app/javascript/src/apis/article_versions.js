import axios from "axios";

const list = articleId => axios.get(`/api/articles/${articleId}/versions`);

const show = (articleId, versionId) =>
  axios.get(`/api/articles/${articleId}/versions/${versionId}`);

const update = (articleId, versionId) =>
  axios.put(`/api/articles/${articleId}/versions/${versionId}`);

const articleVersionsApi = { list, show, update };

export default articleVersionsApi;
