import React from "react";

import PropTypes from "prop-types";

import categoriesReducer from "reducers/categories";

const CategoriesStateContext = React.createContext();
const CategoriesDispatchContext = React.createContext();
const initialState = {
  fetchcategories: null,
  categoriesData: [],
  submitCategory: null,
  handleInputFieldCollapse: null,
};

const CategoriesProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(categoriesReducer, initialState);

  return (
    <CategoriesStateContext.Provider value={state}>
      <CategoriesDispatchContext.Provider value={dispatch}>
        {children}
      </CategoriesDispatchContext.Provider>
    </CategoriesStateContext.Provider>
  );
};

const useCategoriesState = () => {
  const context = React.useContext(CategoriesStateContext);
  if (context === undefined) {
    throw new Error(
      "useCategoriesState must be used within a CategoriesProvider"
    );
  }

  return context;
};

const useCategoriesDispatch = () => {
  const context = React.useContext(CategoriesDispatchContext);
  if (context === undefined) {
    throw new Error(
      "useCategoriesDispatch must be used within a CategoriesProvider"
    );
  }

  return context;
};

const useCategories = () => [useCategoriesState(), useCategoriesDispatch()];

CategoriesProvider.propTypes = {
  children: PropTypes.node,
};

export {
  CategoriesProvider,
  useCategoriesState,
  useCategoriesDispatch,
  useCategories,
};
