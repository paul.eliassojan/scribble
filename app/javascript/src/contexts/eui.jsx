import React from "react";

import PropTypes from "prop-types";

import euiReducer from "reducers/eui";

const EuiStateContext = React.createContext();
const EuiDispatchContext = React.createContext();
const initialState = {
  urlArticleSlug: "",
  urlCategorySlug: "",
};

const EuiProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(euiReducer, initialState);

  return (
    <EuiStateContext.Provider value={state}>
      <EuiDispatchContext.Provider value={dispatch}>
        {children}
      </EuiDispatchContext.Provider>
    </EuiStateContext.Provider>
  );
};

const useEuiState = () => {
  const context = React.useContext(EuiStateContext);
  if (context === undefined) {
    throw new Error("useEuiState must be used within a EuiProvider");
  }

  return context;
};

const useEuiDispatch = () => {
  const context = React.useContext(EuiDispatchContext);
  if (context === undefined) {
    throw new Error("useEuiDispatch must be used within a EuiProvider");
  }

  return context;
};

const useEui = () => [useEuiState(), useEuiDispatch()];

EuiProvider.propTypes = {
  children: PropTypes.node,
};

export { EuiProvider, useEuiState, useEuiDispatch, useEui };
