import React from "react";

import PropTypes from "prop-types";

import articleReducer from "reducers/article";

const ArticleStateContext = React.createContext();
const ArticleDispatchContext = React.createContext();
const initialState = {
  deleteArticle: null,
  isStatusVisible: false,
  status: "draft",
  currentArticleId: "",
};

const ArticleProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(articleReducer, initialState);

  return (
    <ArticleStateContext.Provider value={state}>
      <ArticleDispatchContext.Provider value={dispatch}>
        {children}
      </ArticleDispatchContext.Provider>
    </ArticleStateContext.Provider>
  );
};

const useArticleState = () => {
  const context = React.useContext(ArticleStateContext);
  if (context === undefined) {
    throw new Error("useArticleState must be used within a ArticleProvider");
  }

  return context;
};

const useArticleDispatch = () => {
  const context = React.useContext(ArticleDispatchContext);
  if (context === undefined) {
    throw new Error("useArticleDispatch must be used within a ArticleProvider");
  }

  return context;
};

const useArticle = () => [useArticleState(), useArticleDispatch()];

ArticleProvider.propTypes = {
  children: PropTypes.node,
};

export { ArticleProvider, useArticleState, useArticleDispatch, useArticle };
