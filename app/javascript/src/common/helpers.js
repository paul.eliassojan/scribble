import categoriesApi from "apis/categories";
import organizationApi from "apis/organization";

export const fetchCategories = async () => {
  try {
    const {
      data: { categories },
    } = await categoriesApi.list();
    return categories;
  } catch (error) {
    logger.error(error);
    return [];
  }
};

export const fetchOrganization = async () => {
  try {
    const { data } = await organizationApi.show();
    return data;
  } catch (error) {
    logger.error(error);
    return [];
  }
};
