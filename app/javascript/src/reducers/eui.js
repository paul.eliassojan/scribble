const euiReducer = (state, { type, payload }) => {
  switch (type) {
    case "SET_EUI_SLUG": {
      return {
        urlArticleSlug: payload.urlArticleSlug,
        urlCategorySlug: payload.urlCategorySlug,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${type}`);
    }
  }
};

export default euiReducer;
