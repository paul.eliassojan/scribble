const articleReducer = (state, { type, payload }) => {
  switch (type) {
    case "SET_DELETE_ARTICLE": {
      return {
        handleDeleteArticle: payload.deleteArticle,
      };
    }
    case "SET_ARTICLE_STATUS": {
      return {
        ...state,
        isStatusVisible: payload.isStatusVisible,
        status: payload.status,
      };
    }
    case "SET_ARTICLE_ID": {
      return {
        ...state,
        currentArticleId: payload.currentArticleId,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${type}`);
    }
  }
};

export default articleReducer;
