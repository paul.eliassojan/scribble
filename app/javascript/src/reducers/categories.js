const categoriesReducer = (state, { type, payload }) => {
  switch (type) {
    case "SET_CATEGORIES": {
      return {
        fetchCategories: payload.fetchcategories,
        categoriesData: payload.categoriesData,
      };
    }
    case "SET_SUBMIT_CATEGORY": {
      return {
        ...state,
        handleSubmitCategory: payload.submitCategory,
        handleInputFieldCollapse: payload.handleInputFieldCollapse,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${type}`);
    }
  }
};

export default categoriesReducer;
