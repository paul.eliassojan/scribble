import React from "react";

import NotFound from "images/NotFound";

const PageNotFound = () => (
  <div className="-ml-10 flex h-screen flex-row justify-center">
    <div className="mx-auto -mt-10 flex h-full w-full flex-col justify-center sm:max-w-md">
      <img alt="Logo" src={NotFound} />
    </div>
  </div>
);

export default PageNotFound;
