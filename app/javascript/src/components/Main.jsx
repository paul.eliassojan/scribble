import React, { useEffect, useState } from "react";

import { PageLoader } from "neetoui";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import { fetchOrganization } from "common/helpers";
import Dashboard from "components/Dashboard";
import { EuiProvider } from "contexts/eui";
import { setOrganizationNameToLocalStorage } from "utils/storage";

import PageNotFound from "./Common/PageNotFound";
import PrivateRoute from "./Common/PrivateRoute";
import Login from "./Eui/Login";
import { EUI_ROUTES } from "./routeConstants";

const Main = ({ isLoggedIn }) => {
  const [isPasswordProtected, setIsPasswordProtected] = useState(false);
  const [loading, setLoading] = useState(true);

  const fetchOrganizationData = async () => {
    try {
      const { password_protected, name } = await fetchOrganization();
      setOrganizationNameToLocalStorage(name);
      setIsPasswordProtected(password_protected);
      setLoading(false);
    } catch (error) {
      logger.error(error);
    }
  };

  useEffect(() => {
    fetchOrganizationData();
  });

  if (loading) {
    return (
      <div className="h-screen">
        <PageLoader />
      </div>
    );
  }

  return (
    <EuiProvider>
      <BrowserRouter>
        <ToastContainer />
        <Switch>
          <Route component={Login} path="/login" />
          <Route component={Dashboard} path="/admin" />
          <Route component={PageNotFound} path="/404" />
          {EUI_ROUTES.map(route => (
            <PrivateRoute
              component={route.component}
              condition={isLoggedIn}
              isPasswordProtected={isPasswordProtected}
              key={route.path}
              path={route.path}
              redirectRoute="/login"
            />
          ))}
        </Switch>
      </BrowserRouter>
    </EuiProvider>
  );
};

export default Main;
