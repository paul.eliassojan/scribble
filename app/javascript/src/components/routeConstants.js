import Analytics from "./Dashboard/Analytics";
import Articles from "./Dashboard/Articles";
import AddEdit from "./Dashboard/Articles/AddEdit";
import Settings from "./Dashboard/Settings";
import Eui from "./Eui";

export const ARTICLES_PATH = "/admin";
export const ARTICLE_CREATE_PATH = "/admin/create";
export const ARTICLE_EDIT_PATH = "/admin/edit/:id";
export const SETTINGS_PATH = "/admin/settings";
export const ANALYTICS_PATH = "/admin/analytics";
export const EUI_PATH = "/";
export const EUI_ARTICLE_PATH = "/:slug";

export const DASHBOARD_ROUTES = [
  {
    path: ARTICLES_PATH,
    component: Articles,
  },
  {
    path: ARTICLE_CREATE_PATH,
    component: AddEdit,
  },
  {
    path: ARTICLE_EDIT_PATH,
    component: AddEdit,
  },
  {
    path: SETTINGS_PATH,
    component: Settings,
  },
  {
    path: ANALYTICS_PATH,
    component: Analytics,
  },
];

export const EUI_ROUTES = [
  {
    path: EUI_ARTICLE_PATH,
    component: Eui,
  },
  {
    path: EUI_PATH,
    component: Eui,
  },
];
