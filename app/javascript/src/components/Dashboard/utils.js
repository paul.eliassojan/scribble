import dayjs from "dayjs";

export const formatDate = dateTime => dayjs(dateTime).format("MMMM Do, YYYY");

export const formatDateAndTime = dateTime =>
  dayjs(dateTime).format("h:mm A,  MM/DD/YYYY");

export const convertToLocalTime = dateTime =>
  dayjs(dateTime).format("DD-MM-YYYY hh:mm A");

export const dayBeforeCurrentDay = () => dayjs().add(-1, "day").endOf("day");
