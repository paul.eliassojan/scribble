import React from "react";

import { Route, Switch, Redirect } from "react-router-dom";

import NavBar from "components/Dashboard/NavBar";
import { DASHBOARD_ROUTES, ARTICLES_PATH } from "components/routeConstants";
import { ArticleProvider } from "contexts/article";
import { CategoriesProvider } from "contexts/categories";

const Dashboard = () => (
  <CategoriesProvider>
    <ArticleProvider>
      <NavBar />
      <div className="flex h-screen">
        <Switch>
          {DASHBOARD_ROUTES.map(({ path, component }) => (
            <Route exact component={component} key={path} path={path} />
          ))}
          <Redirect from={ARTICLES_PATH} to="/404" />
        </Switch>
      </div>
    </ArticleProvider>
  </CategoriesProvider>
);

export default Dashboard;
