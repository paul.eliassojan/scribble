import React from "react";

import { Typography, Tag } from "neetoui";

import TooltipWrapper from "components/Common/TooltipWrapper";
import { formatDate } from "components/Dashboard/utils";

export const buildTableColumnData = [
  {
    title: "Title",
    dataIndex: "title",
    key: "title",
    width: "30",
    render: title => (
      <TooltipWrapper
        content={title}
        disabled={title.length >= 15 && true}
        position="top"
      >
        <Typography className="font-medium" style="body1">
          {title.substring(0, 15)} {title.length >= 15 && "..."}
        </Typography>
      </TooltipWrapper>
    ),
  },
  {
    title: "Date",
    dataIndex: "created_at",
    key: "date",
    width: "30",
    render: date => (date === "-" ? date : formatDate(date)),
  },
  {
    title: "Categories",
    dataIndex: "categories",
    key: "categories",
    width: "30",
    render: categories =>
      categories.map((category, index) => (
        <Tag key={index} label={category.title} style="secondary" />
      )),
  },
  {
    title: "Visits",
    dataIndex: "article_visits",
    key: "article_visits",
    width: "30",
    sorter: {
      compare: (a, b) => a.article_visits - b.article_visits,
    },
    render: article_visits => (
      <Typography style="body1">{article_visits}</Typography>
    ),
  },
];

export const buildNestedTableColumnData = [
  {
    title: "Date",
    dataIndex: "date",
    key: "date",
    width: "10",
  },
  {
    title: "Visits",
    dataIndex: "visits",
    key: "visits",
    width: "10",
  },
];
