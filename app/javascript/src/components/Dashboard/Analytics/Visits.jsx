import React, { useState, useEffect } from "react";

import { Table as NeetoUITable, Spinner } from "neetoui";

import articleAnalyticsApi from "apis/article_analytics";

import { buildNestedTableColumnData } from "./utils";

const Visits = ({ articleId }) => {
  const [articeVisits, setArticeVisits] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const fetchArticleVisits = async () => {
    try {
      const {
        data: { article_visits },
      } = await articleAnalyticsApi.show(articleId);
      setArticeVisits(article_visits);
    } catch (error) {
      logger.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchArticleVisits();
  }, []);

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <div className="w-2/5">
      <NeetoUITable
        columnData={buildNestedTableColumnData}
        rowData={articeVisits}
      />
    </div>
  );
};

export default Visits;
