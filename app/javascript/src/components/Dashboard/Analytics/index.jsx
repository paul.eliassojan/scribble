import React, { useEffect, useState } from "react";

import { Table as NeetoUITable, PageLoader } from "neetoui";

import articleAnalyticsApi from "apis/article_analytics";

import { DEFAULT_PAGE_LIMIT } from "./constants";
import { buildTableColumnData } from "./utils";
import Visits from "./Visits";

const Analytics = () => {
  const [analyticsData, setAnalyticsData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [isLoading, setIsLoading] = useState(true);
  const [overallPageSize, setOverallPageSize] = useState(1);

  const fetchArticleAnalytics = async () => {
    try {
      const {
        data: { articles, articles_size },
      } = await articleAnalyticsApi.list(pageNumber, DEFAULT_PAGE_LIMIT);
      setAnalyticsData(articles);
      setOverallPageSize(articles_size);
    } catch (error) {
      logger.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchArticleAnalytics();
  }, [pageNumber]);

  if (isLoading) {
    return (
      <div className="h-screen w-full">
        <PageLoader />
      </div>
    );
  }

  return (
    <div className="mx-20 mt-20">
      <NeetoUITable
        allowRowClick={false}
        columnData={buildTableColumnData}
        currentPageNumber={pageNumber}
        defaultPageSize={DEFAULT_PAGE_LIMIT}
        handlePageChange={value => setPageNumber(value)}
        rowData={analyticsData}
        scroll={{ x: 1300 }}
        totalCount={overallPageSize}
        expandable={{
          rowExpandable: () => true,
          expandedRowRender: record => <Visits articleId={record.id} />,
        }}
      />
    </div>
  );
};

export default Analytics;
