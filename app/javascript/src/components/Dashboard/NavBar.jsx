import React from "react";

import { ExternalLink } from "neetoicons";
import { Typography, Button, Tag } from "neetoui";
import { Link, NavLink } from "react-router-dom";

import { useArticleState } from "contexts/article";

const NavBar = () => {
  const { status, isStatusVisible } = useArticleState();

  return (
    <nav className=" mb-0.5 bg-white">
      <div className="max-w-7xl mx-auto px-2 sm:px-4 lg:px-8">
        <div className="flex h-16 justify-between">
          <div className="flex px-2 lg:px-0">
            <div className="hidden space-x-10 lg:flex">
              <div className="pt-5">
                <Typography style="h4">Scribble</Typography>
              </div>
              <div className="mt-5 space-x-5 font-bold">
                <NavLink
                  exact
                  activeClassName="neeto-ui-text-primary-800"
                  to="/admin"
                >
                  Articles
                </NavLink>
                <NavLink
                  exact
                  activeClassName="neeto-ui-text-primary-800"
                  to="/admin/settings"
                >
                  Settings
                </NavLink>
                <NavLink
                  exact
                  activeClassName="neeto-ui-text-primary-800"
                  to="/admin/analytics"
                >
                  Analytics
                </NavLink>
              </div>
            </div>
          </div>
          <div className="flex items-center justify-end gap-x-4">
            {isStatusVisible && (
              <Tag
                label={status}
                style={status === "draft" ? "warning" : "success"}
                type="outline"
              />
            )}
            <Link target={"_blank"} to="/">
              <Button
                icon={ExternalLink}
                label="Preview"
                size="small"
                style="secondary"
              />
            </Link>
          </div>
        </div>
      </div>
      <hr />
    </nav>
  );
};

export default NavBar;
