import React, { useState, useEffect } from "react";

import { Typography, Button, Tag } from "neetoui";
import { isNil } from "ramda";

import articleVersionsApi from "apis/article_versions";
import { VERSION_HISTORY } from "components/Dashboard/Articles/constants";
import { formatDateAndTime } from "components/Dashboard/utils";
import { useArticleState } from "contexts/article";

import Modal from "./Modal";

const VersionHistory = ({
  fetchArticleAndSetIsLoading,
  fetchCategoriesData,
  currentArticle,
}) => {
  const [articleVersions, setArticleVersions] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [versionId, setVersionId] = useState("");

  const { title, subHeading } = VERSION_HISTORY;
  const { status, updated_at, restore_version_time, is_scheduled } =
    currentArticle;

  const { currentArticleId } = useArticleState();

  const fetchArticleVersions = async () => {
    try {
      const {
        data: { article_versions },
      } = await articleVersionsApi.list(currentArticleId);
      setArticleVersions(article_versions);
    } catch (error) {
      logger.error(error);
    }
  };

  const handleArticleRestore = async () => {
    try {
      await articleVersionsApi.update(currentArticleId, versionId);
    } catch (error) {
      logger.error(error);
    } finally {
      fetchArticleAndSetIsLoading();
      fetchCategoriesData();
      setShowModal(false);
    }
  };

  useEffect(() => {
    fetchArticleVersions();
  }, []);

  return (
    <div className="mx-10 h-screen w-1/2 border-l-2 pl-4">
      <Typography className="mt-5 font-semibold" style="h3">
        {title}
      </Typography>
      <Typography className="mt-3">{subHeading}</Typography>
      <div className="border rounded mt-5 flex space-x-10 bg-teal-100 p-5">
        <div className="flex-col">
          <Typography className="neeto-ui-text-gray-500" style="body2">
            {formatDateAndTime(updated_at)}
          </Typography>
          {isNil(restore_version_time) ? (
            <Typography className="neeto-ui-text-gray-500 mt-2" style="body2">
              Current Version
            </Typography>
          ) : (
            <Typography className="neeto-ui-text-gray-500 mt-2" style="body2">
              (Restored from: {formatDateAndTime(restore_version_time)})
            </Typography>
          )}
          {is_scheduled === true && (
            <Tag className="mt-2" label="Scheduled" size="small" />
          )}
        </div>
        <Typography className="mt-4" style="h5">
          Article {status}
        </Typography>
      </div>
      <div className="overflow-y-scroll" style={{ height: "65%" }}>
        {articleVersions.map(
          ({ created_at, status, id, restore_version_time, is_scheduled }) => (
            <div className="border rounded mt-5 flex space-x-10 p-5" key={id}>
              <div className="flex-col">
                <Typography className="neeto-ui-text-gray-500" style="body2">
                  {formatDateAndTime(created_at)}
                </Typography>
                {!isNil(restore_version_time) && (
                  <Typography
                    className="neeto-ui-text-gray-500 mt-2"
                    style="body2"
                  >
                    (Restored from: {formatDateAndTime(restore_version_time)})
                  </Typography>
                )}
                {is_scheduled === true && (
                  <Tag className="mt-2" label="Scheduled" size="small" />
                )}
              </div>
              <Button
                style="link"
                label={
                  status === "draft"
                    ? `Article ${status}ed`
                    : `Article ${status}`
                }
                onClick={() => {
                  setShowModal(true);
                  setVersionId(id);
                }}
              />
            </div>
          )
        )}
      </div>
      {showModal && (
        <Modal
          articleId={currentArticleId}
          handleArticleRestore={handleArticleRestore}
          setShowModal={setShowModal}
          versionId={versionId}
        />
      )}
    </div>
  );
};

export default VersionHistory;
