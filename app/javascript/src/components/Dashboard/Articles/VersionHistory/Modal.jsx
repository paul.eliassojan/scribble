import React, { useEffect, useState } from "react";

import {
  Typography,
  Button,
  Modal as NeetoModal,
  Input,
  Textarea,
  Select,
} from "neetoui";

import articleVersionsApi from "apis/article_versions";
import { VERSION_HISTORY } from "components/Dashboard/Articles/constants";

const Modal = ({
  setShowModal,
  articleId,
  versionId,
  handleArticleRestore,
}) => {
  const [articleVersion, setArticleVersion] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const { title, subHeading, userAlert } = VERSION_HISTORY;
  const {
    title: articleTitle,
    body: articleBody,
    categories = [],
  } = articleVersion;

  const destructuredCategories = categories.map(({ title, id }) => ({
    label: title,
    value: id,
  }));

  const fetchArticleVersion = async () => {
    try {
      const { data } = await articleVersionsApi.show(articleId, versionId);
      setArticleVersion(data);
    } catch (error) {
      logger.error(error);
    }
  };

  useEffect(() => {
    fetchArticleVersion();
  }, []);

  return (
    <NeetoModal isOpen onClose={() => setShowModal(false)}>
      <NeetoModal.Header>
        <Typography id="dialog1Title" style="h2">
          {title}
        </Typography>
        <Typography className="mt-3" style="body2">
          {subHeading}
        </Typography>
        <Typography className="mt-3 italic" style="body2">
          {userAlert}
        </Typography>
      </NeetoModal.Header>
      <NeetoModal.Body className="space-y-2">
        <div className="flex space-x-2">
          <Input disabled label="Article Title" value={articleTitle} />
          <Select
            isDisabled
            isMulti
            className="w-full flex-grow-0"
            label="Categories"
            value={destructuredCategories}
          />
        </div>
        <Textarea
          disabled
          label="Article Content"
          rows={5}
          size="large"
          value={articleBody}
        />
      </NeetoModal.Body>
      <NeetoModal.Footer className="space-x-2">
        <Button
          label="Restore Version"
          loading={isLoading}
          onClick={() => {
            handleArticleRestore();
            setIsLoading(true);
          }}
        />
        <Button
          label="Cancel"
          style="text"
          onClick={() => setShowModal(false)}
        />
      </NeetoModal.Footer>
    </NeetoModal>
  );
};

export default Modal;
