import React, { useEffect, useState } from "react";

import { PageLoader } from "neetoui";
import { useParams } from "react-router-dom";

import articlesApi from "apis/articles";
import { fetchCategories } from "common/helpers";
import { ARTICLE_FORM_INITIAL_VALUES } from "components/Dashboard/Articles/constants";
import VersionHistory from "components/Dashboard/Articles/VersionHistory";
import { useArticleDispatch } from "contexts/article";

import Form from "./Form";

const AddEdit = () => {
  const [article, setArticle] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [categories, setCategories] = useState([]);
  const [articleStatus, setArticleStatus] = useState("");

  const articleDispatch = useArticleDispatch();
  const { id: articleId } = useParams();
  const isEdit = articleId !== undefined;

  const fetchArticle = async articleId => {
    try {
      const { data } = await articlesApi.show(articleId);
      setArticleStatus(data.status);
      const categoriesData = data.categories.map(({ title, id }) => ({
        label: title,
        value: id,
      }));
      setArticle({ ...data, categories: categoriesData });
    } catch (error) {
      logger.error(error);
    }
  };

  const fetchCategoriesData = async () => {
    const categoriesData = await fetchCategories();
    const processedData = categoriesData?.map(({ id, title }) => ({
      label: title,
      value: id,
    }));
    setCategories(processedData);
  };

  const fetchArticleAndSetIsLoading = async () => {
    await Promise.all([fetchArticle(articleId), setIsLoading(isEdit)]);
    setIsLoading(false);
  };

  useEffect(() => {
    articleDispatch({
      type: "SET_ARTICLE_STATUS",
      payload: { status: articleStatus, isStatusVisible: isEdit },
    });
  }, [isEdit, articleStatus, articleDispatch]);

  useEffect(() => {
    articleDispatch({
      type: "SET_ARTICLE_ID",
      payload: { currentArticleId: articleId },
    });
  }, [articleDispatch]);

  useEffect(() => {
    isEdit ? fetchArticleAndSetIsLoading() : setIsLoading(false);
    fetchCategoriesData();
  }, [articleId, isEdit]);

  if (isLoading) {
    return (
      <div className="h-screen w-full">
        <PageLoader />
      </div>
    );
  }

  return (
    <>
      <Form
        categories={categories}
        initialArticle={isEdit ? article : ARTICLE_FORM_INITIAL_VALUES}
        isEdit={isEdit}
      />
      {isEdit && (
        <VersionHistory
          currentArticle={article}
          fetchArticleAndSetIsLoading={fetchArticleAndSetIsLoading}
          fetchCategoriesData={fetchCategoriesData}
        />
      )}
    </>
  );
};
export default AddEdit;
