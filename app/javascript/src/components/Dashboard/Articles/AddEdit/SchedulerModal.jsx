import React, { useState } from "react";

import { Button, Modal, Typography, DatePicker } from "neetoui";
import { mergeLeft, isNil } from "ramda";
import { useHistory } from "react-router-dom";

import articleSchedulersApi from "apis/article_schedulers";
import articlesApi from "apis/articles";
import TooltipWrapper from "components/Common/TooltipWrapper";
import { dayBeforeCurrentDay } from "components/Dashboard/utils";

const SchedulerModal = ({
  setShowSchedulerModal,
  showSchedulerModal,
  schedulerStatus,
  articleId,
  formValues,
}) => {
  const [scheduledDateTime, setScheduledDateTime] = useState("");
  const [isSubmitDisabled, setIsSubmitDisabled] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const history = useHistory();
  const currentScheduleStatus =
    schedulerStatus === "published" ? "draft" : "published";

  const processFormData = () => {
    const { categories } = formValues;
    const categoryIds = categories.map(({ value }) => value);
    const articleData = mergeLeft(
      {
        category_ids: categoryIds,
      },
      formValues
    );
    return articleData;
  };

  const disabledDate = current => current && current < dayBeforeCurrentDay();

  const handleSubmit = async () => {
    const articleData = processFormData();
    const schedulerData = {
      scheduling_date_time: scheduledDateTime,
      status: currentScheduleStatus,
    };
    setIsLoading(true);
    try {
      isNil(articleId)
        ? await articlesApi.createSchedule({ schedulerData, articleData })
        : await articleSchedulersApi.create(articleId, schedulerData);
      history.push("/admin");
    } catch (error) {
      logger.error(error);
    } finally {
      setShowSchedulerModal(false);
      setIsLoading(false);
      setIsSubmitDisabled(true);
    }
  };

  return (
    <Modal
      isOpen={showSchedulerModal}
      onClose={() => setShowSchedulerModal(false)}
    >
      <Modal.Header>
        <Typography style="h2">Schedule Article</Typography>
      </Modal.Header>
      <Modal.Body className="flex space-x-2">
        <DatePicker
          showTime
          disabledDate={disabledDate}
          label="Date"
          picker="date"
          placeholder="Select date and time"
          timeFormat="HH"
          type="date"
          onChange={(_, dateTime) => {
            setScheduledDateTime(dateTime);
            setIsSubmitDisabled(false);
          }}
        />
      </Modal.Body>
      <Modal.Footer className="flex space-x-2">
        <TooltipWrapper
          content="Select a valid date and time"
          disabled={isSubmitDisabled}
          position="left"
        >
          <Button
            disabled={isSubmitDisabled}
            loading={isLoading}
            label={
              schedulerStatus === "published"
                ? "Unpublish Later"
                : "Publish Later"
            }
            onClick={handleSubmit}
          />
        </TooltipWrapper>
        <Button
          label="Cancel"
          style="text"
          onClick={() => setShowSchedulerModal(false)}
        />
      </Modal.Footer>
    </Modal>
  );
};

export default SchedulerModal;
