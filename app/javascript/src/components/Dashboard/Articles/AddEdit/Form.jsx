import React, { useEffect, useState } from "react";

import { Formik, Form as FormikForm } from "formik";
import { Clock } from "neetoicons";
import { Button } from "neetoui";
import { Input, Textarea, Select } from "neetoui/formik";
import { isEmpty, mergeLeft } from "ramda";
import { useHistory } from "react-router-dom";

import articleSchedulersApi from "apis/article_schedulers";
import articlesApi from "apis/articles";
import SchedulerModal from "components/Dashboard/Articles/AddEdit/SchedulerModal";
import {
  ARTICLE_STATUS_ITEMS,
  ARTICLE_FORM_VALIDATION_SCHEMA,
} from "components/Dashboard/Articles/constants";
import { useArticleState } from "contexts/article";

import ActionDropdown from "./ActionDropdown";
import ListSchedules from "./ListSchedules";

const Form = ({ isEdit, initialArticle, categories }) => {
  const [savePreference, setSavePreference] = useState("");
  const [submitted, setSubmitted] = useState(false);
  const [showSchedulerModal, setShowSchedulerModal] = useState(false);
  const [formValues, setFormValues] = useState({});
  const [schedulersData, setSchedulersData] = useState([]);
  const [schedulerStatus, setSchedulerStatus] = useState("");

  const history = useHistory();
  const { currentArticleId, status: articleStatus } = useArticleState();

  const initialStatusState = isEmpty(articleStatus)
    ? ARTICLE_STATUS_ITEMS.find(({ state }) => state === "draft")?.state
    : articleStatus;

  const fetchArticleSchedules = async () => {
    try {
      const {
        data: { article_schedules },
      } = await articleSchedulersApi.list(currentArticleId);
      setSchedulersData(article_schedules);
    } catch (error) {
      logger.error(error);
    }
  };

  const handleSubmitArticle = async values => {
    const { categories } = values;
    const categoryIds = categories.map(({ value }) => value);
    const articleData = mergeLeft(
      {
        status: savePreference,
        category_ids: categoryIds,
      },
      values
    );
    setSubmitted(true);
    try {
      isEdit
        ? await articlesApi.update(articleData, currentArticleId)
        : await articlesApi.create({
            payload: articleData,
          });
      history.push("/admin");
    } catch (err) {
      logger.error(err);
    }
  };

  useEffect(() => {
    setSavePreference(initialStatusState);
    isEmpty(schedulersData) && setSchedulerStatus(initialStatusState);
  }, [initialStatusState, schedulersData]);

  return (
    <>
      <Formik
        initialValues={initialArticle}
        validateOnBlur={submitted}
        validationSchema={ARTICLE_FORM_VALIDATION_SCHEMA}
        onSubmit={handleSubmitArticle}
      >
        {({ isSubmitting, handleSubmit, dirty, isValid, values }) => (
          <FormikForm className="mx-40 mt-12 w-full space-y-4">
            <div className="flex w-full space-x-2">
              <Input
                required
                className="w-2/3 flex-grow-0"
                label="Article Title"
                name="title"
                placeholder="Enter title"
              />
              <Select
                isClearable
                isMulti
                isSearchable
                required
                className="w-full flex-grow-0"
                label="Categories"
                name="categories"
                options={categories}
                placeholder="Select Category"
              />
            </div>
            <Textarea
              required
              className="w-full flex-grow-0"
              label="Article Body"
              name="body"
              placeholder="Enter note description"
              rows={10}
            />
            <div className="clearfix space-x-2 pt-5">
              <div className="float-left flex">
                <ActionDropdown
                  dirty={dirty}
                  handleSubmit={handleSubmit}
                  initialStatusState={initialStatusState}
                  isEdit={isEdit}
                  isSubmitting={isSubmitting}
                  isValid={isValid}
                  savePreference={savePreference}
                  schedulersData={schedulersData}
                  setSavePreference={setSavePreference}
                />
                <Button
                  label="Cancel"
                  style="link neeto-ui-text-gray-700"
                  to={"/admin"}
                  type="reset"
                />
              </div>
              <div className="float-right">
                <Button
                  disabled={schedulersData.length === 2}
                  icon={Clock}
                  iconPosition="right"
                  style="secondary"
                  label={
                    schedulerStatus === "published"
                      ? "Unpublish Later"
                      : "Publish Later"
                  }
                  onClick={() => {
                    setShowSchedulerModal(true);
                    setFormValues(values);
                  }}
                />
              </div>
            </div>
            <ListSchedules
              articleStatus={articleStatus}
              currentArticleId={currentArticleId}
              fetchArticleSchedules={fetchArticleSchedules}
              schedulersData={schedulersData}
              setSchedulerStatus={setSchedulerStatus}
            />
          </FormikForm>
        )}
      </Formik>
      <SchedulerModal
        articleId={currentArticleId}
        formValues={formValues}
        schedulerStatus={schedulerStatus}
        setShowSchedulerModal={setShowSchedulerModal}
        showSchedulerModal={showSchedulerModal}
      />
    </>
  );
};

export default Form;
