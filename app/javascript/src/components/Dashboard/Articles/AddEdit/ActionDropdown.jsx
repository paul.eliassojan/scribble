import React, { useState } from "react";

import {
  ActionDropdown as NeetoActionDropdown,
  Modal,
  Typography,
  Button,
} from "neetoui";
import { isEmpty } from "ramda";

import TooltipWrapper from "components/Common/TooltipWrapper";
import { ARTICLE_STATUS_ITEMS } from "components/Dashboard/Articles/constants";

const ActionDropdown = ({
  initialStatusState,
  savePreference,
  handleSubmit,
  isValid,
  dirty,
  isEdit,
  isSubmitting,
  setSavePreference,
  schedulersData,
}) => {
  const [showModal, setShowModal] = useState(false);
  const { Menu, MenuItem } = NeetoActionDropdown;

  const filterArticleStatus = newState =>
    ARTICLE_STATUS_ITEMS.find(({ state }) => state === newState)?.label;

  const handleSchedulerAlert = articleState => {
    setShowModal(true);
    setSavePreference(articleState);
  };

  return (
    <>
      <TooltipWrapper
        content="Make changes to save"
        position="left"
        disabled={
          isEdit
            ? savePreference === initialStatusState && !(dirty && isValid)
            : !(dirty && isValid)
        }
      >
        <NeetoActionDropdown
          loading={isSubmitting}
          type="submit"
          buttonProps={{
            disabled: isEdit
              ? savePreference === initialStatusState && !(dirty && isValid)
              : !(dirty && isValid),
          }}
          label={
            savePreference === "draft"
              ? filterArticleStatus("draft")
              : filterArticleStatus("published")
          }
          onClick={handleSubmit}
        >
          <Menu>
            {ARTICLE_STATUS_ITEMS.map(({ label, state }) => (
              <MenuItem.Button
                key={state}
                onClick={() => {
                  initialStatusState !== state && !isEmpty(schedulersData)
                    ? handleSchedulerAlert(state)
                    : setSavePreference(state);
                }}
              >
                {label}
              </MenuItem.Button>
            ))}
          </Menu>
        </NeetoActionDropdown>
      </TooltipWrapper>
      <Modal closeButton={false} isOpen={showModal}>
        <Modal.Header>
          <Typography style="h2">Schedule Already Exists</Typography>
        </Modal.Header>
        <Modal.Body className="space-y-2">
          <Typography lineHeight="normal" style="body2">
            A schedule with status <b>{savePreference}</b> already exists.
            Changing article status will delete that schedule.
          </Typography>
        </Modal.Body>
        <Modal.Footer className="space-x-2">
          <Button
            label="Proceed"
            style="danger"
            onClick={() => setShowModal(false)}
          />
          <Button
            label="Cancel"
            style="text"
            onClick={() => {
              setShowModal(false);
              setSavePreference(initialStatusState);
            }}
          />
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ActionDropdown;
