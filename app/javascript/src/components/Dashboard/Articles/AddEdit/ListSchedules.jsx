import React, { useEffect, useState } from "react";

import { Info, Delete } from "neetoicons";
import { Callout, Typography, Button, Alert } from "neetoui";
import { isNil } from "ramda";

import articleSchedulersApi from "apis/article_schedulers";
import { SCHEDULER_DELETE_ALERT } from "components/Dashboard/Articles/constants";
import { convertToLocalTime } from "components/Dashboard/utils";

const ListSchedules = ({
  schedulersData,
  fetchArticleSchedules,
  currentArticleId,
  setSchedulerStatus,
  articleStatus,
}) => {
  const [showDeleteSchedulerAlert, setShowDeleteSchedulerAlert] =
    useState(false);
  const [schedulerId, setSchedulerId] = useState("");
  const [currentScheduleStatus, setCurrentScheduleStatus] = useState("");

  const { withContradictionMessage, withoutContradictionMessage } =
    SCHEDULER_DELETE_ALERT;

  const handleAlert = (schedulerId, status) => {
    setShowDeleteSchedulerAlert(true),
      setSchedulerId(schedulerId),
      setCurrentScheduleStatus(status);
  };

  const handleDelete = async () => {
    try {
      await articleSchedulersApi.destroy(currentArticleId, schedulerId);
    } catch (error) {
      logger.error(error);
    } finally {
      fetchArticleSchedules();
    }
  };

  useEffect(() => {
    !isNil(currentArticleId) && fetchArticleSchedules();
  }, []);

  return (
    <div className="pt-8">
      {schedulersData.map(({ scheduling_date_time, status, id }) => {
        setSchedulerStatus(status);
        return (
          <Callout className="mt-4" icon={Info} key={id}>
            <div className="clearfix w-full">
              <div className="float-left mt-1">
                <Typography className="italic" style="body3">
                  Article is scheduled to {status} at &nbsp;
                  {convertToLocalTime(scheduling_date_time)}
                </Typography>
              </div>
              <div className="float-right">
                <Button
                  icon={Delete}
                  size="small"
                  style="danger-text"
                  onClick={() => {
                    handleAlert(id, status);
                  }}
                />
              </div>
            </div>
          </Callout>
        );
      })}
      <Alert
        isOpen={showDeleteSchedulerAlert}
        title="Do you want to delete this schedule?"
        message={
          currentScheduleStatus !== articleStatus && schedulersData.length > 1
            ? withContradictionMessage
            : withoutContradictionMessage
        }
        onClose={() => setShowDeleteSchedulerAlert(false)}
        onSubmit={() => {
          setShowDeleteSchedulerAlert(false), handleDelete();
        }}
      />
    </div>
  );
};

export default ListSchedules;
