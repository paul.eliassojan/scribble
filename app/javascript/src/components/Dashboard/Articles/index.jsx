import React, { useState, useEffect } from "react";

import { Typography, PageLoader } from "neetoui";
import { Container } from "neetoui/layouts";
import Pluralize from "react-pluralize";

import articlesApi from "apis/articles";
import { fetchCategories } from "common/helpers";
import useDebounce from "components/hooks/useDebounce";
import { useArticleDispatch } from "contexts/article";

import ActionBlock from "./ActionBlock";
import { DEFAULT_PAGE_LIMIT, TABLE_HEADERS } from "./constants";
import MenuBar from "./MenuBar";
import Table from "./Table";

const Articles = () => {
  const [articlesCount, setArticlesCount] = useState({});
  const [categories, setCategories] = useState([]);
  const [filteredArticles, setFilteredArticles] = useState([]);
  const [tableColsToBeDisplayed, setTableColsToBeDisplayed] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(1);
  const [filteredArticlesCount, setFilteredArticlesCount] = useState(1);

  const articleDispatch = useArticleDispatch();
  const debouncedSearchTerm = useDebounce(searchQuery, 400);
  const tableHeaders = TABLE_HEADERS.map(({ title }) => title);

  const fetchArticles = async (articleStatus, categoryTitle) => {
    try {
      const {
        data: { articles, articles_count, filtered_articles_size },
      } = await articlesApi.list(
        articleStatus,
        categoryTitle,
        searchQuery,
        pageNumber,
        DEFAULT_PAGE_LIMIT
      );
      setArticlesCount(articles_count);
      setFilteredArticles(articles);
      setFilteredArticlesCount(filtered_articles_size);
    } catch (error) {
      logger.error(error);
    }
  };

  const fetchCategoriesData = async () => {
    try {
      const categoriesData = await fetchCategories();
      setCategories(categoriesData);
    } catch (error) {
      logger.error(error);
    }
  };

  const handleDeleteArticle = async articleId => {
    try {
      await articlesApi.destroy(articleId);
    } catch (error) {
      logger.error(error);
    } finally {
      fetchArticles();
      fetchCategoriesData();
    }
  };

  const fetchArticlesAndFetchCategoriesData = async () => {
    Promise.all([fetchArticles(), fetchCategoriesData()]);
    setIsLoading(false);
  };

  const articleDispatchAndSetTableColsToBeDisplayed = async () => {
    Promise.all([
      articleDispatch({
        type: "SET_DELETE_ARTICLE",
        payload: { deleteArticle: handleDeleteArticle },
      }),

      setTableColsToBeDisplayed(tableHeaders),
    ]);
  };

  useEffect(() => {
    fetchArticlesAndFetchCategoriesData();
  }, [debouncedSearchTerm, pageNumber]);

  useEffect(() => {
    articleDispatchAndSetTableColsToBeDisplayed();
  }, []);

  if (isLoading) {
    return (
      <div className="h-screen w-full">
        <PageLoader />
      </div>
    );
  }

  return (
    <>
      <MenuBar
        categories={categories}
        fetchArticles={fetchArticles}
        fetchCategoriesData={fetchCategoriesData}
        menuArticlesCount={articlesCount}
        setPageNumber={setPageNumber}
      />
      <Container>
        <ActionBlock
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
          setTableColsToBeDisplayed={setTableColsToBeDisplayed}
          tableColsToBeDisplayed={tableColsToBeDisplayed}
          tableHeaders={tableHeaders}
        />
        <Typography className="mb-5" style="h3">
          <Pluralize count={filteredArticlesCount} singular={"Article"} />
        </Typography>
        <Table
          filteredArticlesCount={filteredArticlesCount}
          pageNumber={pageNumber}
          rowData={filteredArticles}
          setPageNumber={setPageNumber}
          tableColsToBeDisplayed={tableColsToBeDisplayed}
        />
      </Container>
    </>
  );
};

export default Articles;
