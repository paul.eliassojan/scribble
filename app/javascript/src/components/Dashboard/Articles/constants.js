import * as yup from "yup";

export const ARTICLE_STATUS_ITEMS = [
  { state: "draft", label: "Save as Draft" },
  { state: "published", label: "Save as Published" },
];
export const ARTICLE_FORM_INITIAL_VALUES = {
  title: "",
  categories: [],
  body: "",
};

export const ARTICLE_FORM_VALIDATION_SCHEMA = yup.object().shape({
  title: yup
    .string()
    .matches(/^[a-zA-Z0-9]/, "Title is not valid")
    .required("Title is required"),
  body: yup.string().required("Body is required"),
  categories: yup
    .array()
    .of(
      yup.object().shape({
        label: yup.string().required(),
        value: yup.string().required(),
      })
    )
    .min(1, "Category is required")
    .required("Category is required."),
});

export const CATEGORY_FORM_VALIDATION_SCHEMA = yup.object().shape({
  title: yup
    .string()
    .matches(/^[a-zA-Z0-9]/, "Category is not valid")
    .required("Category is required"),
});

export const CATEGORY_FORM_INITIAL_VALUES = {
  title: "",
};

export const TABLE_HEADERS = [
  {
    id: 0,
    title: "Title",
  },
  {
    id: 1,
    title: "Last Published Date",
  },
  {
    id: 2,
    title: "Author",
  },
  {
    id: 3,
    title: "Category",
  },
  {
    id: 4,
    title: "Status",
  },
  {
    id: 4,
    title: "",
  },
];

export const VERSION_HISTORY = {
  title: "Version History",
  subHeading: "Version history of Setting up an article in Scribble.",
  userAlert:
    " Note: Article will be moved to a draft state when restored. So if the upcoming schedule is draft it will get deleted.",
};

export const DEFAULT_PAGE_LIMIT = 8;

export const SCHEDULER_DELETE_ALERT = {
  withContradictionMessage:
    "Deteting this schedule will delete other schedule too of this article because of status contradiction.",
  withoutContradictionMessage:
    "This schedule will be deleted permanently. This action can't be undone.",
};
