import React, { useEffect, useState } from "react";

import { Search, Plus, Close } from "neetoicons";
import { Typography } from "neetoui";
import { MenuBar as Menu } from "neetoui/layouts";

import categoriesApi from "apis/categories";

import AddCategoryForm from "./AddCategoryForm";
import { handleCategorySearch, handleCategoryfilter } from "./utils";

const MenuBar = ({
  menuArticlesCount,
  categories,
  fetchCategoriesData,
  fetchArticles,
  setPageNumber,
}) => {
  const [isSearchCollapsed, setIsSearchCollapsed] = useState(true);
  const [isAddCategoryCollapsed, setIsAddCategoryCollapsed] = useState(false);
  const [articleStatus, setArticleStatus] = useState("all");
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [searchCategoryQuery, setSearchCategoryQuery] = useState("");
  const [filteredCategories, setFilteredCategories] = useState([]);

  const handleSubmit = async (values, { resetForm }) => {
    try {
      await categoriesApi.create(values);
    } catch (error) {
      logger.error(error);
    } finally {
      fetchCategoriesData();
      setIsAddCategoryCollapsed(
        isAddCategoryCollapsed => !isAddCategoryCollapsed
      );
      resetForm({});
    }
  };

  const capitalizeFirstLetter = string =>
    string.charAt(0).toUpperCase() + string.slice(1);

  const handleCloseCategorySearchField = () => {
    setIsSearchCollapsed(prevIsSearchCollapsed => !prevIsSearchCollapsed);
    setFilteredCategories(categories);
    setSearchCategoryQuery("");
  };

  const fetchArticlesAndSetFilteredCategories = async () => {
    await Promise.all([
      setFilteredCategories(categories),
      fetchArticles(
        articleStatus === "all" ? "" : articleStatus,
        selectedCategories
      ),
    ]);
  };

  useEffect(() => {
    fetchArticlesAndSetFilteredCategories();
  }, [selectedCategories, articleStatus, categories]);

  return (
    <Menu className="flex" showMenu={() => true} title="Articles">
      {Object.keys(menuArticlesCount).map(status => (
        <Menu.Block
          active={articleStatus.includes(status)}
          count={menuArticlesCount[status]}
          key={status}
          label={capitalizeFirstLetter(status)}
          onClick={() => {
            setArticleStatus(status);
            setPageNumber(1);
          }}
        />
      ))}
      <Menu.SubTitle
        iconProps={[
          {
            icon: Search,
            onClick: () => setIsSearchCollapsed(false),
          },
          {
            icon: isAddCategoryCollapsed ? Close : Plus,
            onClick: () =>
              setIsAddCategoryCollapsed(
                isAddCategoryCollapsed => !isAddCategoryCollapsed
              ),
          },
        ]}
      >
        <Typography
          component="h4"
          style="h5"
          textTransform="uppercase"
          weight="bold"
        >
          Categories
        </Typography>
      </Menu.SubTitle>
      <Menu.Search
        autoFocus
        collapse={isSearchCollapsed}
        value={searchCategoryQuery}
        onChange={event =>
          handleCategorySearch(
            event.target.value,
            setSearchCategoryQuery,
            categories,
            setFilteredCategories
          )
        }
        onCollapse={() => {
          handleCloseCategorySearchField();
        }}
        onKeyDown={event =>
          event.key === "Escape" && handleCloseCategorySearchField()
        }
      />
      {isAddCategoryCollapsed && (
        <AddCategoryForm
          handleSubmit={handleSubmit}
          setIsAddCategoryCollapsed={setIsAddCategoryCollapsed}
        />
      )}
      {filteredCategories.map(({ id, title, articles_count }) => (
        <Menu.Block
          active={selectedCategories.includes(title)}
          count={articles_count}
          key={id}
          label={title}
          onClick={() => {
            handleCategoryfilter(
              title,
              selectedCategories,
              setSelectedCategories,
              setIsSearchCollapsed
            );
            setPageNumber(1);
          }}
        />
      ))}
    </Menu>
  );
};

export default MenuBar;
