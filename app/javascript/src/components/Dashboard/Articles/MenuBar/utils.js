export const handleCategorySearch = (
  searchCategoryQuery,
  setSearchCategoryQuery,
  categories,
  setFilteredCategories
) => {
  setSearchCategoryQuery(searchCategoryQuery);
  const filteredCategories = categories.filter(({ title }) =>
    title.toLowerCase().includes(searchCategoryQuery.toLowerCase())
  );
  setFilteredCategories(filteredCategories);
};

export const handleCategoryfilter = (
  categoryTitle,
  selectedCategories,
  setSelectedCategories,
  setIsSearchCollapsed
) => {
  if (!selectedCategories.includes(categoryTitle)) {
    setSelectedCategories(prevCurrentCategory => [
      ...prevCurrentCategory,
      categoryTitle,
    ]);
  } else {
    setSelectedCategories(
      selectedCategories.filter(category => category !== categoryTitle)
    );
  }
  setIsSearchCollapsed(true);
};
