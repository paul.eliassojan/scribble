import React, { useState } from "react";

import { Formik, Form as FormikForm } from "formik";
import { Check } from "neetoicons";
import { Button } from "neetoui";
import { Input } from "neetoui/formik";

import TooltipWrapper from "components/Common/TooltipWrapper";
import {
  CATEGORY_FORM_INITIAL_VALUES,
  CATEGORY_FORM_VALIDATION_SCHEMA,
} from "components/Dashboard/Articles/constants";

const AddCategoryForm = ({ handleSubmit, setIsAddCategoryCollapsed }) => {
  const [submitted, setSubmitted] = useState(false);

  return (
    <Formik
      initialValues={CATEGORY_FORM_INITIAL_VALUES}
      validateOnBlur={submitted}
      validationSchema={CATEGORY_FORM_VALIDATION_SCHEMA}
      onSubmit={handleSubmit}
    >
      {({ isSubmitting, dirty, isValid }) => (
        <FormikForm className="mb-5 flex flex-row space-x-2">
          <Input
            autoFocus
            required
            name="title"
            placeholder="Add Category"
            suffix={
              <TooltipWrapper
                content="Type to save"
                disabled={!(dirty && isValid)}
                position="top"
              >
                <Button
                  disabled={!(dirty && isValid)}
                  icon={Check}
                  loading={isSubmitting}
                  size="small"
                  style="text"
                  type="submit"
                  onClick={() => setSubmitted(true)}
                />
              </TooltipWrapper>
            }
            onKeyDown={event =>
              event.key === "Escape" && setIsAddCategoryCollapsed(false)
            }
          />
        </FormikForm>
      )}
    </Formik>
  );
};

export default AddCategoryForm;
