import React from "react";

import { Button, Dropdown, Checkbox } from "neetoui";
import { Header as NeetoUIHeader } from "neetoui/layouts";

const ActionBlock = ({
  setTableColsToBeDisplayed,
  tableColsToBeDisplayed,
  tableHeaders,
  searchQuery,
  setSearchQuery,
}) => {
  const { Menu, MenuItem } = Dropdown;

  const handleDropdown = event => {
    const {
      target: { checked, id },
    } = event;
    if (checked) {
      setTableColsToBeDisplayed(prevState => [...prevState, id]);
    } else {
      setTableColsToBeDisplayed(prevState =>
        prevState.filter(item => item !== id)
      );
    }
  };

  return (
    <NeetoUIHeader
      actionBlock={
        <>
          <Dropdown
            buttonStyle="secondary"
            closeOnSelect={false}
            label="Columns"
          >
            <Menu>
              {tableHeaders
                .filter(columnHeaders => columnHeaders !== "")
                .map(columnHeaders => (
                  <MenuItem.Button key={columnHeaders}>
                    <Checkbox
                      checked={tableColsToBeDisplayed.includes(columnHeaders)}
                      id={columnHeaders}
                      label={columnHeaders}
                      onChange={handleDropdown}
                    />
                  </MenuItem.Button>
                ))}
            </Menu>
          </Dropdown>
          <Button label="Add new article" to={"admin/create"} />
        </>
      }
      searchProps={{
        onChange: event => setSearchQuery(event.target.value),
        value: searchQuery,
        placeholder: "Search title",
      }}
    />
  );
};
export default ActionBlock;
