import React from "react";

import { Delete, Edit } from "neetoicons";
import { Typography, Tag, Button } from "neetoui";

import TooltipWrapper from "components/Common/TooltipWrapper";
import { formatDate } from "components/Dashboard/utils";

export const buildTableColumnData = (setArticleId, setIsDeleteAlertOpen) => [
  {
    title: "Title",
    dataIndex: "title",
    key: "title",
    width: 30,
    render: title => (
      <TooltipWrapper
        content={title}
        disabled={title.length >= 15 && true}
        position="top"
      >
        <Typography className="font-medium" style="body1">
          {title.substring(0, 15)} {title.length >= 15 && "..."}
        </Typography>
      </TooltipWrapper>
    ),
  },
  {
    title: "Last Published Date",
    dataIndex: "last_published_at",
    key: "date",
    width: "15%",
    render: date => (date === "-" ? date : formatDate(date)),
  },
  {
    title: "Author",
    dataIndex: "user",
    key: "user",
    width: 30,
    render: user => user.name,
  },
  {
    title: "Category",
    dataIndex: "categories",
    key: "categories",
    width: 30,
    render: categories =>
      categories.map(({ title, id }) => (
        <Tag key={id} label={title} style="secondary" />
      )),
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
    width: 30,
  },
  {
    title: "",
    dataIndex: "id",
    key: "id",
    width: 10,
    render: id => (
      <div className="space-x-2">
        <Button
          icon={Delete}
          size="small"
          style="secondary"
          onClick={() => {
            setIsDeleteAlertOpen(prevDeleteAlertOpen => !prevDeleteAlertOpen);
            setArticleId(id);
          }}
        />
        <Button
          icon={Edit}
          size="small"
          style="secondary"
          to={`/admin/edit/${id}`}
        />
      </div>
    ),
  },
];
