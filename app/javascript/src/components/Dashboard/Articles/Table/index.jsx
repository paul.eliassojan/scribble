import React, { useState } from "react";

import { Table as NeetoUITable, Alert } from "neetoui";
import { isNil } from "ramda";

import { DEFAULT_PAGE_LIMIT } from "components/Dashboard/Articles/constants";
import { useArticleState } from "contexts/article";

import { buildTableColumnData } from "./utils";

const Table = ({
  rowData,
  tableColsToBeDisplayed,
  setPageNumber,
  pageNumber,
  filteredArticlesCount,
}) => {
  const [isDeleteAlertOpen, setIsDeleteAlertOpen] = useState(false);
  const [articleId, setArticleId] = useState("");

  const { handleDeleteArticle } = useArticleState();

  const columnData = buildTableColumnData(
    setArticleId,
    setIsDeleteAlertOpen
  ).filter(({ title }) => tableColsToBeDisplayed.includes(title));

  const filteredRowData = rowData.map(article => {
    if (isNil(article.last_published_at)) {
      return { ...article, last_published_at: "-" };
    }

    return article;
  });

  return (
    <>
      <NeetoUITable
        allowRowClick={false}
        columnData={columnData}
        currentPageNumber={pageNumber}
        defaultPageSize={DEFAULT_PAGE_LIMIT}
        handlePageChange={value => setPageNumber(value)}
        rowData={filteredRowData}
        totalCount={filteredArticlesCount}
      />
      {isDeleteAlertOpen && (
        <Alert
          isOpen
          message="Are you sure you want to delete this article? This action cannot be undone."
          title="Delete Article"
          onClose={() =>
            setIsDeleteAlertOpen(prevDeleteAlertOpen => !prevDeleteAlertOpen)
          }
          onSubmit={() => {
            handleDeleteArticle(articleId),
              setIsDeleteAlertOpen(
                prevIsDeleteAlertOpen => !prevIsDeleteAlertOpen
              );
          }}
        />
      )}
    </>
  );
};

export default Table;
