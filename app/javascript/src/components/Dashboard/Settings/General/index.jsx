import React, { useEffect, useState } from "react";

import { Typography, PageLoader } from "neetoui";

import organizationApi from "apis/organization";
import { fetchOrganization } from "common/helpers";
import { GENERAL } from "components/Dashboard/Settings/constants";

import Form from "./Form";

const General = () => {
  const [organization, setOrganization] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isPasswordCheck, setIsPasswordCheck] = useState(false);
  const [isPasswordFieldDisabled, setIsPasswordFieldDisabled] = useState(true);
  const [isSubscribed, setIsSubscribed] = useState(false);

  const { title, subHeading, inputFieldDescription } = GENERAL;

  const fetchOrganizationData = async () => {
    try {
      const organizationData = await fetchOrganization();
      setOrganization(organizationData);
      const { password_protected } = organizationData;
      setIsLoading(false);
      setIsPasswordCheck(password_protected);
      !password_protected && setIsPasswordFieldDisabled(false);
    } catch (error) {
      logger.error(error);
    }
  };

  const handleSubmit = async (values, { resetForm }) => {
    const { name, password_protected } = values;
    try {
      await organizationApi.update(values);
    } catch (error) {
      logger.error(error);
    } finally {
      setIsPasswordCheck(false);
      setIsPasswordFieldDisabled(true);
      resetForm({ values: { name, password_protected } });
      fetchOrganizationData();
      setIsSubscribed(false);
    }
  };

  useEffect(() => {
    fetchOrganizationData();
  }, []);

  if (isLoading) {
    return (
      <div className="h-screen w-full">
        <PageLoader />
      </div>
    );
  }

  return (
    <div className="mt-20 ml-20 flex w-full justify-center">
      <div>
        <div className="mb-10 space-y-2 align-middle">
          <Typography style="h2">{title}</Typography>
          <Typography className="neeto-ui-text-gray-500" style="body1">
            {subHeading}
          </Typography>
        </div>
        <Form
          handleSubmit={handleSubmit}
          initialOrganization={organization}
          inputFieldDescription={inputFieldDescription}
          isPasswordCheck={isPasswordCheck}
          isPasswordFieldDisabled={isPasswordFieldDisabled}
          isSubscribed={isSubscribed}
          setIsPasswordCheck={setIsPasswordCheck}
          setIsPasswordFieldDisabled={setIsPasswordFieldDisabled}
          setIsSubscribed={setIsSubscribed}
        />
      </div>
    </div>
  );
};

export default General;
