import React, { useState } from "react";

import { Formik, Form as FormikForm } from "formik";
import { Close } from "neetoicons";
import { Typography, Button } from "neetoui";
import { Input, Checkbox } from "neetoui/formik";

import TooltipWrapper from "components/Common/TooltipWrapper";
import { ORGANIZATION_FORM_VALIDATION_SCHEMA } from "components/Dashboard/Settings/constants";

const Form = ({
  handleSubmit,
  initialOrganization,
  isPasswordCheck,
  setIsPasswordCheck,
  inputFieldDescription,
  setIsPasswordFieldDisabled,
  isPasswordFieldDisabled,
  isSubscribed,
  setIsSubscribed,
}) => {
  const [submitted, setSubmitted] = useState(false);
  const [isPasswordFieldOpen, setIsPasswordFieldOpen] = useState(false);

  const handleCancel = resetForm => {
    isPasswordFieldOpen &&
      setIsPasswordCheck(prevIsPasswordCheck => !prevIsPasswordCheck);
    setIsSubscribed(false);
    !isPasswordFieldOpen && setIsPasswordFieldDisabled(true);
    resetForm({});
  };

  const handlePasswordCheck = event => {
    const {
      target: { value },
    } = event;
    value === "false"
      ? (setIsPasswordCheck(true),
        setIsSubscribed(prevIsSubscribed => !prevIsSubscribed))
      : (setIsPasswordCheck(false),
        setIsSubscribed(prevIsSubscribed => !prevIsSubscribed));
  };

  const handlePasswordChange = () => {
    setIsPasswordFieldDisabled(false),
      setIsSubscribed(true),
      setIsPasswordFieldOpen(false);
  };

  return (
    <Formik
      initialValues={initialOrganization}
      validateOnBlur={submitted}
      validationSchema={ORGANIZATION_FORM_VALIDATION_SCHEMA}
      onSubmit={handleSubmit}
    >
      {({ isSubmitting, resetForm, isValid, dirty }) => (
        <div className="w-3/4">
          <FormikForm>
            <Input
              required
              label="Site Name"
              name="name"
              suffix={
                !isSubscribed && (
                  <Button
                    disabled={!(dirty && isValid)}
                    icon={Close}
                    loading={isSubmitting}
                    size="small"
                    style="text"
                    onClick={() => resetForm({})}
                  />
                )
              }
            />
            <Typography className="neeto-ui-text-gray-500" style="body2">
              {inputFieldDescription}
            </Typography>
            <div className="mt-5">
              <hr />
            </div>
            <Checkbox
              className="mt-5"
              label="Password Protect Knowledge Base"
              name="password_protected"
              onClick={event => {
                handlePasswordCheck(event);
                setIsPasswordFieldOpen(true);
              }}
            />
            {isPasswordCheck && (
              <div className="flex">
                <TooltipWrapper
                  content="Click change password to edit"
                  disabled={isPasswordFieldDisabled}
                  position="left"
                >
                  <div className="flex w-full">
                    <Input
                      required
                      className="mt-5 w-1/2"
                      disabled={isPasswordFieldDisabled}
                      label="Password"
                      name="password"
                      placeholder="******"
                      type="password"
                    />
                  </div>
                </TooltipWrapper>
                {isPasswordFieldDisabled && (
                  <Button
                    className="h-18 mt-10 ml-4"
                    label="Change Password"
                    size="small"
                    style="link"
                    onClick={() => handlePasswordChange()}
                  />
                )}
              </div>
            )}
            <div className="mt-10 flex">
              <TooltipWrapper
                content="Make changes to save"
                disabled={!(dirty && isValid)}
                position="left"
              >
                <Button
                  disabled={!(dirty && isValid)}
                  label="Save Changes"
                  loading={isSubmitting}
                  style="primary"
                  type="submit"
                  onClick={() => setSubmitted(true)}
                />
              </TooltipWrapper>
              {isSubscribed && (
                <Button
                  className="neeto-ui-text-black ml-5"
                  label="Cancel"
                  style="link"
                  type="reset"
                  onClick={() => handleCancel(resetForm)}
                />
              )}
            </div>
          </FormikForm>
        </div>
      )}
    </Formik>
  );
};

export default Form;
