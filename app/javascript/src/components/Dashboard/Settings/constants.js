import * as yup from "yup";

import General from "./General";
import ManageCategories from "./ManageCategories";
import Redirections from "./Redirections";

export const SETTINGS_NAVLINKS = [
  {
    key: "general",
    label: "General",
    description: "Page Title, Brand Name and Meta Description",
    path: "/admin/settings?tab=general",
    component: General,
  },
  {
    key: "redirections",
    label: "Redirections",
    description: "Create and configure redirection rules",
    path: "/admin/settings?tab=redirections",
    component: Redirections,
  },
  {
    key: "manage_categories",
    label: "Manage categories",
    description: "Edit and Reorder KB Structure",
    path: "/admin/settings?tab=manage_categories",
    component: ManageCategories,
  },
];

export const GENERAL = {
  title: "General Settings",
  subHeading: "Configure general attributes of scribble.",
  inputFieldDescription:
    "Customize the site name which is used to show the site name in Open Graph Tags. ",
};

export const REDIRECTIONS = {
  title: "Redirections",
  subHeading:
    "Create and configure redirection rules to send users from old links to new links. All redirections are performed with 301 status codes to be SEO friendly.",
};

export const MANAGE_CATEGORIES = {
  title: "Manage Categories",
  subHeading: "Create and configure the categories inside your scribble.",
};

export const ORGANIZATION_FORM_VALIDATION_SCHEMA = yup.object().shape(
  {
    name: yup.string().required("Name field is required"),
    password: yup.string().when("password", {
      is: exists => exists,
      then: yup
        .string()
        .required("Password is required")
        .matches(/\d/, "Must contain atlest one digit")
        .matches(/[a-zA-Z]/, "Must contain atlest one letter")
        .min(6, "Must contain 6 or more characters"),
    }),
  },
  [["password", "password"]]
);

export const REDIRECTIONS_FORM_INITIAL_VALUES = {
  to_path: "",
  from_path: "",
};

export const REDIRECTIONS_FORM_VALIDATION_SCHEMA = yup.object().shape({
  to_path: yup
    .string()
    .required("To Path is required")
    .matches(/^[a-zA-Z0-9]/, "Invalid path"),
  from_path: yup
    .string()
    .required("From Path is required")
    .matches(/^[a-zA-Z0-9]/, "Invalid path"),
});

export const MANAGE_CATEGORIES_FORM_INITIAL_VALUES = {
  title: "",
};

export const MANAGE_CATEGORIES_DELETE_FORM_INITIAL_VALUES = {
  categories: [],
};

export const MANAGE_CATEGORIES_DELETE_FORM_VALIDATION_SCHEMA = yup
  .object()
  .shape({
    categories: yup
      .object()
      .shape({
        label: yup.string().required(),
        value: yup.string().required(),
      })
      .nullable()
      .required("Category is required."),
  });

export const MANAGE_CATEGORIES_FORM_VALIDATION_SCHEMA = yup.object().shape({
  title: yup
    .string()
    .matches(/^[a-zA-Z0-9]/, "Category is not valid")
    .required("Field is required"),
});
