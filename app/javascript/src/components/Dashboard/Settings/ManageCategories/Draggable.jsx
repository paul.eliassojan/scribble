import React from "react";

import { Delete, Edit, Reorder } from "neetoicons";
import { Typography, Button } from "neetoui";
import { Draggable as BeautifulDndDraggable } from "react-beautiful-dnd";

import TooltipWrapper from "components/Common/TooltipWrapper";

import AddEditForm from "./AddEditForm";

const Draggable = ({
  draggableId,
  index,
  isEdit,
  categoryId,
  categoryTitle,
  handleDelete,
  handleEdit,
  articlesCount,
}) => (
  <BeautifulDndDraggable draggableId={draggableId.toString()} index={index}>
    {provided => (
      <div
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        className="clearfix max-w-2xl bg-white"
        ref={provided.innerRef}
      >
        {isEdit && categoryId === draggableId ? (
          <div className="p-5">
            <AddEditForm initialCategories={{ title: categoryTitle }} />
          </div>
        ) : (
          <>
            <div className="float-left flex">
              <Reorder className="mt-5 w-4" />
              <Typography className=" p-5 font-semibold">
                {categoryTitle}
              </Typography>
            </div>
            <div className="float-right mt-5 flex">
              <TooltipWrapper
                content="General category cannot be deleted"
                disabled={categoryTitle.toUpperCase() === "GENERAL"}
                position="top"
              >
                <Button
                  disabled={categoryTitle.toUpperCase() === "GENERAL"}
                  icon={Delete}
                  iconPosition="left"
                  size="small"
                  style="text"
                  onClick={() =>
                    handleDelete(categoryTitle, articlesCount, draggableId)
                  }
                />
              </TooltipWrapper>
              <Button
                icon={Edit}
                iconPosition="left"
                size="small"
                style="text"
                onClick={() => handleEdit(draggableId, categoryTitle)}
              />
            </div>
          </>
        )}
      </div>
    )}
  </BeautifulDndDraggable>
);

export default Draggable;
