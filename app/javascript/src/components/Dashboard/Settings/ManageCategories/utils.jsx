import React from "react";

import Pluralize from "react-pluralize";

export const deleteCalloutMessage = (title, articlesCount, categoriesData) => (
  <>
    {categoriesData.length === 1 && articlesCount > 0 && (
      <span>
        All articles under <b>{title}</b> will be moved to a new category called
        General.
      </span>
    )}
    {categoriesData.length > 1 && articlesCount > 0 && (
      <span>
        Category <b>{title}</b> has&nbsp;
        {<Pluralize count={articlesCount} singular={"Article"} />}. Before this
        category can be deleted, these articles need to be moved to another
        category.
      </span>
    )}
    {articlesCount === 0 && (
      <span>
        Category <b>{title}</b> has {articlesCount} articles.
      </span>
    )}
  </>
);

export const deleteModalHeader = title => (
  <span>
    You are permantly deleting the <b>{title}</b>. This action cannot be undone.
    Are you sure you wish to continue?
  </span>
);
