import React, { useEffect, useState } from "react";

import { Plus } from "neetoicons";
import { Typography, Button } from "neetoui";
import { DragDropContext, Droppable } from "react-beautiful-dnd";

import categoriesApi from "apis/categories";
import {
  MANAGE_CATEGORIES_FORM_INITIAL_VALUES,
  MANAGE_CATEGORIES,
} from "components/Dashboard/Settings/constants";
import { useCategoriesState, useCategoriesDispatch } from "contexts/categories";

import AddEditForm from "./AddEditForm";
import CategoryDelete from "./Delete";
import Draggable from "./Draggable";

const List = ({ handleUpdateOnDrag }) => {
  const [dragCategories, setDragCategories] = useState([]);
  const [isCreate, setIsCreate] = useState(true);
  const [isEdit, setIsEdit] = useState(false);
  const [categoryId, setCategoryId] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [currentCategory, setCurrentCategory] = useState({});

  const { title, subHeading } = MANAGE_CATEGORIES;
  const { fetchCategories, categoriesData } = useCategoriesState();

  const categoriesDispatch = useCategoriesDispatch();

  const handleOnDragEnd = result => {
    if (!result.destination) return;

    const categoryItems = Array.from(dragCategories);
    const [reorderedItem] = categoryItems.splice(result.source.index, 1);
    categoryItems.splice(result.destination.index, 0, reorderedItem);

    const categoryId = result.draggableId;
    const {
      destination: { index },
    } = result;

    setDragCategories(categoryItems);
    handleUpdateOnDrag(categoryId, { position: index + 1 });
  };

  const handleInputFieldCollapse = () =>
    isEdit
      ? setIsEdit(prevIsEdit => !prevIsEdit)
      : setIsCreate(prevIsCreate => !prevIsCreate);

  const handleEdit = id => {
    setIsCreate(true);
    setIsEdit(true);
    setCategoryId(id);
  };

  const handleSubmit = async values => {
    try {
      isEdit
        ? await categoriesApi.update({ id: categoryId, payload: values })
        : await categoriesApi.create(values);
    } catch (error) {
      logger.error(error);
    } finally {
      handleInputFieldCollapse();
      fetchCategories();
    }
  };

  const handleDelete = (title, articlesCount, id) => {
    setCurrentCategory(currentCategory => ({
      ...currentCategory,
      title,
      articles_count: articlesCount,
      id,
    }));
    setShowModal(prevShowModal => !prevShowModal);
  };

  const setDragCategoriesAndCategoriesDispatch = async () => {
    Promise.all([
      setDragCategories(categoriesData),
      categoriesDispatch({
        type: "SET_SUBMIT_CATEGORY",
        payload: { submitCategory: handleSubmit, handleInputFieldCollapse },
      }),
    ]);
  };

  useEffect(() => {
    setDragCategoriesAndCategoriesDispatch();
  }, [categoriesData, categoriesDispatch, categoryId, isEdit]);

  return (
    <div className="mt-20 flex w-full justify-center">
      <div>
        <div className="mb-10 space-y-2 align-middle">
          <Typography style="h2">{title}</Typography>
          <Typography className="neeto-ui-text-gray-500" style="body1">
            {subHeading}
          </Typography>
        </div>
        {isCreate ? (
          <Button
            icon={Plus}
            iconPosition="left"
            label="Add new category"
            size="small"
            style="link"
            onClick={() => {
              setIsCreate(prevIsCreate => !prevIsCreate);
              setIsEdit(false);
            }}
          />
        ) : (
          <AddEditForm
            initialCategories={MANAGE_CATEGORIES_FORM_INITIAL_VALUES}
          />
        )}
        <div className="mt-10">
          <hr />
          <DragDropContext onDragEnd={handleOnDragEnd}>
            <Droppable droppableId="categories">
              {provided => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {dragCategories.map(
                    ({ title, id, articles_count }, index) => (
                      <div key={id}>
                        <Draggable
                          articlesCount={articles_count}
                          categoryId={categoryId}
                          categoryTitle={title}
                          draggableId={id}
                          handleDelete={handleDelete}
                          handleEdit={handleEdit}
                          index={index}
                          isEdit={isEdit}
                        />
                        <hr />
                        {showModal && (
                          <CategoryDelete
                            currentCategory={currentCategory}
                            setShowModal={setShowModal}
                            showModal={showModal}
                          />
                        )}
                      </div>
                    )
                  )}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
      </div>
    </div>
  );
};

export default List;
