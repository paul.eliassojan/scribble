import React, { useState } from "react";

import { Formik, Form as FormikForm } from "formik";
import { Check, Close } from "neetoicons";
import { Button } from "neetoui";
import { Input } from "neetoui/formik";

import TooltipWrapper from "components/Common/TooltipWrapper";
import { MANAGE_CATEGORIES_FORM_VALIDATION_SCHEMA } from "components/Dashboard/Settings/constants";
import { useCategoriesState } from "contexts/categories";

const AddEditForm = ({ initialCategories }) => {
  const [submitted, setSubmitted] = useState(false);

  const { handleSubmitCategory, handleInputFieldCollapse } =
    useCategoriesState();

  return (
    <Formik
      initialValues={initialCategories}
      validateOnBlur={submitted}
      validationSchema={MANAGE_CATEGORIES_FORM_VALIDATION_SCHEMA}
      onSubmit={handleSubmitCategory}
    >
      {({ isSubmitting, isValid, dirty }) => (
        <div className="w-3/4">
          <FormikForm>
            <Input
              autoFocus
              required
              name="title"
              placeholder="Input Category"
              suffix={
                <>
                  <TooltipWrapper
                    content="Make changes to save"
                    disabled={!(dirty && isValid)}
                    position="top"
                  >
                    <Button
                      disabled={!(dirty && isValid)}
                      icon={Check}
                      loading={isSubmitting}
                      size="small"
                      style="text"
                      type="submit"
                      onClick={() => setSubmitted(true)}
                    />
                  </TooltipWrapper>
                  <Button
                    icon={Close}
                    size="small"
                    style="text"
                    type="reset"
                    onClick={handleInputFieldCollapse}
                  />
                </>
              }
              onKeyDown={event =>
                event.key === "Escape" && handleInputFieldCollapse()
              }
            />
          </FormikForm>
        </div>
      )}
    </Formik>
  );
};

export default AddEditForm;
