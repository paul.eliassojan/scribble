import React, { useEffect, useState } from "react";

import { Formik, Form as FormikForm } from "formik";
import { Warning } from "neetoicons";
import { Modal, Button, Typography, Callout } from "neetoui";
import { Select } from "neetoui/formik";
import { isNil } from "ramda";

import categoriesApi from "apis/categories";
import {
  MANAGE_CATEGORIES_DELETE_FORM_INITIAL_VALUES,
  MANAGE_CATEGORIES_DELETE_FORM_VALIDATION_SCHEMA,
} from "components/Dashboard/Settings/constants";
import { useCategoriesState } from "contexts/categories";

import { deleteCalloutMessage, deleteModalHeader } from "./utils";

const Delete = ({ showModal, setShowModal, currentCategory }) => {
  const [submitted, setSubmitted] = useState(false);
  const [categories, setCategories] = useState([]);

  const { fetchCategories, categoriesData } = useCategoriesState();

  const {
    title,
    articles_count: articlesCount,
    id: currentCategoryId,
  } = currentCategory;

  const processCategoriesData = () => {
    const filteredCategories = categoriesData.filter(
      ({ id }) => id !== currentCategoryId
    );

    const destructureFilteredCategories = filteredCategories.map(
      ({ id, title }) => ({
        label: title,
        value: id,
      })
    );
    setCategories(destructureFilteredCategories);
  };

  const handleSubmit = async values => {
    const {
      categories: { value },
    } = values;

    try {
      await categoriesApi.destroy(
        currentCategoryId,
        isNil(value) ? currentCategoryId : value
      );
    } catch (error) {
      logger.error(error);
    } finally {
      setShowModal(prevShowModal => !prevShowModal);
      fetchCategories();
    }
  };

  useEffect(() => {
    processCategoriesData();
  }, []);

  return (
    <Modal
      className="w-full"
      isOpen={showModal}
      onClose={() => setShowModal(false)}
    >
      <Modal.Header>
        <Typography id="dialog1Title" style="h2">
          Delete Category
        </Typography>
      </Modal.Header>
      <Formik
        initialValues={MANAGE_CATEGORIES_DELETE_FORM_INITIAL_VALUES}
        validateOnBlur={submitted}
        validateOnChange={submitted}
        validationSchema={
          categoriesData.length > 1 &&
          articlesCount > 0 &&
          MANAGE_CATEGORIES_DELETE_FORM_VALIDATION_SCHEMA
        }
        onSubmit={handleSubmit}
      >
        {({ isSubmitting, isValid, dirty }) => (
          <FormikForm>
            <Modal.Body className="space-y-2">
              <Typography lineHeight="normal" style="body2">
                {deleteModalHeader(title)}
              </Typography>
              <Callout className="w-54" icon={Warning} style="danger">
                {deleteCalloutMessage(title, articlesCount, categoriesData)}
              </Callout>
              {categoriesData.length > 1 && articlesCount > 0 && (
                <Select
                  isClearable
                  isSearchable
                  required
                  label="Select a category to move these articles into"
                  name="categories"
                  options={categories}
                  placeholder="Select Category"
                />
              )}
            </Modal.Body>
            <Modal.Footer className="space-x-2">
              <Button
                label="Proceed"
                loading={isSubmitting}
                style="danger"
                type="submit"
                disabled={
                  categoriesData.length > 1 &&
                  articlesCount > 0 &&
                  !(dirty && isValid)
                }
                onClick={() => setSubmitted(true)}
              />
              <Button
                label="Cancel"
                style="text"
                type="reset"
                onClick={() => setShowModal(prevShowModal => !prevShowModal)}
              />
            </Modal.Footer>
          </FormikForm>
        )}
      </Formik>
    </Modal>
  );
};

export default Delete;
