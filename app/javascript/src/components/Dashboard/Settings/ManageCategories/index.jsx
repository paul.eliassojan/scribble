import React, { useEffect, useState } from "react";

import { PageLoader } from "neetoui";

import categoriesApi from "apis/categories";
import { fetchCategories } from "common/helpers";
import { useCategoriesDispatch } from "contexts/categories";

import List from "./List";

const ManageCategories = () => {
  const [categories, setCategories] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const categoriesDispatch = useCategoriesDispatch();

  const fetchCategoriesData = async () => {
    try {
      const categoriesData = await fetchCategories();
      setCategories(categoriesData);
      setIsLoading(false);
    } catch (error) {
      logger.error(error);
    }
  };

  const handleUpdateOnDrag = async (id, values) => {
    try {
      await categoriesApi.update({ id, payload: values, quiet: true });
    } catch (error) {
      logger.error(error);
    } finally {
      fetchCategoriesData();
    }
  };

  useEffect(() => {
    categoriesDispatch({
      type: "SET_CATEGORIES",
      payload: {
        fetchcategories: fetchCategoriesData,
        categoriesData: categories,
      },
    });
  }, [categories, categoriesDispatch]);

  useEffect(() => {
    fetchCategoriesData();
  }, []);

  if (isLoading) {
    return (
      <div className="h-screen w-full">
        <PageLoader />
      </div>
    );
  }

  return <List handleUpdateOnDrag={handleUpdateOnDrag} />;
};

export default ManageCategories;
