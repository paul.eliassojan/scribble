import React, { useState, useEffect } from "react";

import { MenuBar as Menu } from "neetoui/layouts";
import queryString from "query-string";
import { useHistory } from "react-router-dom";

import { SETTINGS_NAVLINKS } from "./constants";
import { getActiveNavLink } from "./utils";

const MenuBar = () => {
  const { tab } = queryString.parse(location.search);

  const history = useHistory();

  const [activeNavlink, setActiveNavlink] = useState(
    () => getActiveNavLink(tab) || SETTINGS_NAVLINKS[0]
  );
  useEffect(() => history.push(activeNavlink?.path), [activeNavlink]);

  if (location.state?.resetTab) {
    location.state.resetTab = null;
    setActiveNavlink(() => getActiveNavLink(tab));
  }

  return (
    <>
      <Menu showMenu>
        {SETTINGS_NAVLINKS.map(navlink => (
          <Menu.Item
            active={tab === navlink.key}
            description={navlink.description}
            key={navlink.key}
            label={navlink.label}
            onClick={() => setActiveNavlink(navlink)}
          />
        ))}
      </Menu>
      {<activeNavlink.component />}
    </>
  );
};

export default MenuBar;
