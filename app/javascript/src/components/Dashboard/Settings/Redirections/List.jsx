import React, { useState } from "react";

import { Delete, Edit } from "neetoicons";
import { Typography, Button, Alert } from "neetoui";

const List = ({
  URL,
  fromPath,
  toPath,
  handleDelete,
  handleEdit,
  redirectionId,
}) => {
  const [isAlertOpen, setIsAlertOpen] = useState(false);

  return (
    <>
      <div className="w-64">
        <Typography style="body2">{`${URL}/${fromPath}`}</Typography>
      </div>
      <div className="w-36 pl-5">
        <Typography style="body2">{`${URL}/${toPath}`}</Typography>
      </div>
      <div className="flex space-x-1 pl-10">
        <Button
          icon={Delete}
          size="small"
          style="secondary"
          onClick={() => setIsAlertOpen(prevIsAlertOpen => !prevIsAlertOpen)}
        />
        <Button
          icon={Edit}
          size="small"
          style="secondary"
          onClick={() => handleEdit(redirectionId)}
        />
      </div>
      {isAlertOpen && (
        <Alert
          isOpen
          message="Are you sure you want to delete this redirection? This action cannot be undone."
          title="Delete Redirection"
          onClose={() => setIsAlertOpen(prevIsAlertOpen => !prevIsAlertOpen)}
          onSubmit={() => handleDelete(redirectionId)}
        />
      )}
    </>
  );
};

export default List;
