import React, { useState } from "react";

import { Formik, Form as FormikForm } from "formik";
import { Check, Close } from "neetoicons";
import { Button } from "neetoui";
import { Input } from "neetoui/formik";

import TooltipWrapper from "components/Common/TooltipWrapper";
import { REDIRECTIONS_FORM_VALIDATION_SCHEMA } from "components/Dashboard/Settings/constants";

const Form = ({
  URL,
  handleSubmit,
  initialRedirectionPaths,
  handleInputFieldCollapse,
  isEdit,
}) => {
  const [submitted, setSubmitted] = useState(false);

  return (
    <Formik
      initialValues={initialRedirectionPaths}
      validateOnBlur={submitted}
      validationSchema={REDIRECTIONS_FORM_VALIDATION_SCHEMA}
      onSubmit={handleSubmit}
    >
      {({ isSubmitting, isValid, dirty }) => (
        <FormikForm>
          <div
            className="flex flex-row space-x-2"
            onKeyDown={event =>
              event.key === "Escape" && handleInputFieldCollapse()
            }
          >
            <div className="w-3/5">
              <Input
                autoFocus
                required
                name="from_path"
                placeholder="from path"
                prefix={`${URL}/`}
              />
            </div>
            <div className="w-36 pl-5">
              <Input
                required
                name="to_path"
                placeholder="to path"
                prefix={`${URL}/`}
              />
            </div>
            <div className="flex space-x-1 pl-10">
              <TooltipWrapper
                content="Make changes to save"
                disabled={!(dirty && isValid)}
                position="top"
              >
                <Button
                  disabled={!(dirty && isValid)}
                  icon={Check}
                  loading={isSubmitting}
                  size="small"
                  style="secondary"
                  type="submit"
                  onClick={() => setSubmitted(true)}
                />
              </TooltipWrapper>
              {isEdit && (
                <Button
                  icon={Close}
                  size="small"
                  style="secondary"
                  type="reset"
                  onClick={handleInputFieldCollapse}
                />
              )}
            </div>
          </div>
        </FormikForm>
      )}
    </Formik>
  );
};

export default Form;
