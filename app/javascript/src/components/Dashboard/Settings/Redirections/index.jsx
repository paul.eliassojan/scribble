import React, { useEffect, useState } from "react";

import { Plus, Close } from "neetoicons";
import { Typography, Button, PageLoader } from "neetoui";

import redirectionsApi from "apis/redirections";
import {
  REDIRECTIONS_FORM_INITIAL_VALUES,
  REDIRECTIONS,
} from "components/Dashboard/Settings/constants";

import Form from "./Form";
import List from "./List";

const Redirections = () => {
  const [redirectionsData, setRedirectionsData] = useState([]);
  const [isCreate, setIsCreate] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [redirectionId, setRedirectionId] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  const { title, subHeading } = REDIRECTIONS;
  const URL = window.origin;

  const fetchRedirectionsData = async () => {
    try {
      const {
        data: { redirections },
      } = await redirectionsApi.list();
      setRedirectionsData(redirections);
      setIsLoading(false);
    } catch (error) {
      logger.error(error);
    }
  };

  const handleInputFieldCollapse = () =>
    isEdit
      ? setIsEdit(prevIsEdit => !prevIsEdit)
      : setIsCreate(prevIsCreate => !prevIsCreate);

  const handleSubmit = async values => {
    try {
      isEdit
        ? await redirectionsApi.update(redirectionId, values)
        : await redirectionsApi.create(values);
    } catch (error) {
      logger.error(error);
    } finally {
      fetchRedirectionsData();
      handleInputFieldCollapse();
    }
  };

  const handleDelete = async id => {
    try {
      await redirectionsApi.destroy(id);
    } catch (error) {
      logger.error(error);
    } finally {
      fetchRedirectionsData();
    }
  };

  const handleEdit = id => {
    setRedirectionId(id);
    setIsCreate(false);
    setIsEdit(prevIsEdit => !prevIsEdit);
  };

  useEffect(() => {
    fetchRedirectionsData();
  }, []);

  if (isLoading) {
    return (
      <div className="h-screen w-full">
        <PageLoader />
      </div>
    );
  }

  return (
    <div className="ml-48 mt-10 flex w-full">
      <div>
        <div className="mb-10 space-y-2">
          <Typography style="h2">{title}</Typography>
          <Typography className="neeto-ui-text-gray-500 w-3/4" style="body1">
            {subHeading}
          </Typography>
        </div>
        <div className="border pb-60 mt-10 max-w-2xl bg-indigo-400 bg-opacity-25 p-6">
          <div className="flex space-x-10">
            <Typography className="neeto-ui-text-gray-500 pl-10" style="h4">
              From Path
            </Typography>
            <Typography className="neeto-ui-text-gray-500 pl-40" style="h4">
              To Path
            </Typography>
            <Typography className="neeto-ui-text-gray-500 pl-32" style="h4">
              Actions
            </Typography>
          </div>
          {redirectionsData.map(({ from_path, to_path, id }) => (
            <div className="mt-6 max-w-2xl bg-white p-6" key={id}>
              <div className="flex flex-row space-x-2">
                {isEdit && redirectionId === id ? (
                  <Form
                    isEdit
                    URL={URL}
                    handleInputFieldCollapse={handleInputFieldCollapse}
                    handleSubmit={handleSubmit}
                    initialRedirectionPaths={{ to_path, from_path }}
                  />
                ) : (
                  <List
                    URL={URL}
                    fromPath={from_path}
                    handleDelete={handleDelete}
                    handleEdit={handleEdit}
                    redirectionId={id}
                    toPath={to_path}
                  />
                )}
              </div>
            </div>
          ))}
          {isCreate && (
            <div className="mt-6 max-w-2xl bg-white p-6">
              <Form
                URL={URL}
                handleInputFieldCollapse={handleInputFieldCollapse}
                handleSubmit={handleSubmit}
                initialRedirectionPaths={REDIRECTIONS_FORM_INITIAL_VALUES}
                isEdit={false}
              />
            </div>
          )}
          <div className="mt-5">
            <Button
              icon={isCreate ? Close : Plus}
              iconPosition="left"
              label={isCreate ? "Cancel" : "Add Redirection"}
              size="small"
              style="link"
              onClick={() => {
                setIsCreate(prevIsCreate => !prevIsCreate);
                setIsEdit(false);
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Redirections;
