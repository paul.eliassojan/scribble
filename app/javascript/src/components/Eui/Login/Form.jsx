import React, { useState } from "react";

import { Formik, Form as FormikForm } from "formik";
import { Button, Typography } from "neetoui";
import { Input } from "neetoui/formik";

import TooltipWrapper from "components/Common/TooltipWrapper";
import {
  LOGIN_FORM_INITIAL_VALUES,
  LOGIN_FORM_VALIDATION_SCHEMA,
} from "components/Eui/constants";
import LoginLogo from "images/LoginLogo";

const Form = ({ organizationName, handleSubmit }) => {
  const [submitted, setSubmitted] = useState(false);

  return (
    <div className="flex h-screen w-screen flex-row items-center justify-center overflow-y-auto overflow-x-hidden bg-gray-100 p-6">
      <div className="mx-auto flex h-full w-full flex-col items-center justify-center sm:max-w-md">
        <img alt="Logo" src={LoginLogo} />
        <div className="-ml-6 items-start">
          <Typography
            className="mt-10 mb-2 text-center text-2xl text-gray-800"
            style="h2"
          >
            {organizationName} is password protected!
          </Typography>
          <Typography style="body2">
            Enter the password to gain access to {organizationName}.
          </Typography>
        </div>
        <Formik
          initialValues={LOGIN_FORM_INITIAL_VALUES}
          validateOnBlur={submitted}
          validationSchema={LOGIN_FORM_VALIDATION_SCHEMA}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting, isValid, dirty }) => (
            <FormikForm className=" w-full space-y-6 p-8">
              <Input
                autoFocus
                required
                data-cy="login-password-text-field"
                label="Password"
                name="password"
                placeholder="******"
                type="password"
              />
              <TooltipWrapper
                content="Type password to save"
                disabled={!(dirty && isValid)}
                position="left"
              >
                <Button
                  className="h-8 "
                  data-cy="login-submit-button"
                  disabled={!(dirty && isValid)}
                  label="Continue"
                  loading={isSubmitting}
                  type="submit"
                  onClick={() => setSubmitted(true)}
                />
              </TooltipWrapper>
            </FormikForm>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default Form;
