import React from "react";

import authApi from "apis/Public/auth";
import NavBar from "components/Eui/NavBar";
import { getFromLocalStorage } from "utils/storage";

import LoginForm from "./Form";

const Login = ({ ...props }) => {
  const organizationName = getFromLocalStorage("organizationName");

  const handleSubmit = async values => {
    const newValues = { ...values, name: organizationName };
    try {
      await authApi.login(newValues);
      window.location.href = props?.location?.fromPath || "/";
    } catch (error) {
      logger.error(error);
    }
  };

  return (
    <>
      <NavBar
        isSearchBoxVisible={false}
        organizationName={organizationName}
        setShowSearchModal={false}
      />
      <LoginForm
        handleSubmit={handleSubmit}
        organizationName={organizationName}
      />
    </>
  );
};

export default Login;
