import React from "react";

import { Typography } from "neetoui";

import NoDataFound from "images/NoDataFound";

const NoData = () => (
  <div className="-ml-10 flex h-screen flex-row justify-center">
    <div className="mx-auto -mt-10 flex h-full w-full flex-col justify-center sm:max-w-sm">
      <img alt="Logo" src={NoDataFound} />
      <Typography
        className="mt-10 mb-2 text-center text-2xl text-gray-800"
        style="h2"
      >
        No Published Articles Found!!!!
      </Typography>
    </div>
  </div>
);

export default NoData;
