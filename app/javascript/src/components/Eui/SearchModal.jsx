import React, { useEffect, useState } from "react";

import { Search } from "neetoicons";
import { Modal, Input, Typography, Tag, Spinner } from "neetoui";
import { isEmpty } from "ramda";
import { Link, useHistory } from "react-router-dom";

import publicArticlesApi from "apis/Public/articles";
import useDebounce from "components/hooks/useDebounce";
import useKeyPress from "components/hooks/useKeyPress";

import {
  keyDownPressed,
  keyUpPressed,
  keyEnterPressed,
  showArticle,
} from "./utils";

const SearchModal = ({ showSearchModal, setShowSearchModal }) => {
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredArticles, setFilteredArticles] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [currentArticlePosition, setCurrentArticlePosition] = useState(0);
  const [selectedArticle, setSelectedArticle] = useState({});

  const arrowUpPressed = useKeyPress("ArrowUp");
  const arrowDownPressed = useKeyPress("ArrowDown");
  const enterPress = useKeyPress("Enter");

  const debouncedSearchTerm = useDebounce(searchQuery, 400);
  const history = useHistory();

  const fetchPublicArticles = async () => {
    try {
      const {
        data: { articles },
      } = await publicArticlesApi.list(searchQuery);
      setFilteredArticles(articles.flat());
    } catch (error) {
      logger.error(error);
    } finally {
      setIsLoading(false);
      setCurrentArticlePosition(0);
    }
  };

  const handleModalClose = () => {
    setShowSearchModal(false);
    setSearchQuery("");
    setFilteredArticles([]);
  };

  useEffect(() => {
    keyDownPressed(
      filteredArticles,
      arrowDownPressed,
      setCurrentArticlePosition
    );
  }, [arrowDownPressed]);

  useEffect(() => {
    keyUpPressed(filteredArticles, arrowUpPressed, setCurrentArticlePosition);
  }, [arrowUpPressed]);

  useEffect(() => {
    keyEnterPressed(
      filteredArticles,
      enterPress,
      setSelectedArticle,
      currentArticlePosition
    );
  }, [currentArticlePosition, enterPress]);

  useEffect(() => {
    showArticle(
      selectedArticle,
      history,
      setShowSearchModal,
      setSelectedArticle,
      setCurrentArticlePosition
    );
  }, [currentArticlePosition, enterPress]);

  useEffect(() => {
    fetchPublicArticles();
  }, [debouncedSearchTerm, showSearchModal]);

  return (
    <Modal
      closeButton={false}
      isOpen={showSearchModal}
      onClose={() => handleModalClose()}
    >
      <Input
        placeholder="Search for an article"
        prefix={<Search />}
        size="large"
        suffix={isLoading && <Spinner />}
        type="search"
        value={searchQuery}
        onKeyDown={event => event.key === "Escape" && setShowSearchModal(false)}
        onChange={event => {
          setSearchQuery(event.target.value);
          setIsLoading(true);
        }}
      />
      <div className="rounded bg-white p-2">
        {!isEmpty(filteredArticles) ? (
          filteredArticles.map(
            (
              { article_title, category_title, article_slug, category_slug },
              index
            ) => (
              <div className="bg-white p-4 pt-4" key={index}>
                <Link to={`/${article_slug}?category=${category_slug}`}>
                  <div
                    className="mb-3 flex justify-between"
                    style={{
                      cursor: "pointer",
                      color: index === currentArticlePosition ? "red" : "black",
                    }}
                    onClick={() => setShowSearchModal(false)}
                  >
                    <Typography style="h4">{article_title}</Typography>
                    <Tag label={category_title} />
                  </div>
                </Link>
                <hr />
              </div>
            )
          )
        ) : (
          <Typography className="pl-2" style="body2">
            {searchQuery && !isLoading && "No articles found"}
          </Typography>
        )}
        {debouncedSearchTerm === "" && (
          <Typography className="mt-2 ml-2" style="body3">
            Search for more articles....
          </Typography>
        )}
        <div className="mt-4 ml-4 flex">
          <kbd className="border rounded bg-gray-300	text-lg">&uarr;</kbd>
          <kbd className="border rounded ml-2 bg-gray-300	text-lg">&darr;</kbd>
          <Typography className="ml-2 mt-1" style="body3">
            to navigate
          </Typography>
          <kbd className="border rounded ml-4	bg-gray-300">Enter</kbd>
          <Typography className="ml-2 mt-1" style="body3">
            to select
          </Typography>
        </div>
      </div>
    </Modal>
  );
};

export default SearchModal;
