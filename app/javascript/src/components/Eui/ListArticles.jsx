import React, { useEffect, useState } from "react";

import { Typography } from "neetoui";
import { isNil } from "ramda";
import { Link, useHistory } from "react-router-dom";

import publicCategoriesApi from "apis/Public/categories";

const ListArticles = ({ currentCategorySlug, urlSlug, urlCategorySlug }) => {
  const [publicArticles, setPublicArticles] = useState([]);

  const history = useHistory();

  const fetchPublicArticles = async () => {
    try {
      const {
        data: { category_articles },
      } = await publicCategoriesApi.articlesList(currentCategorySlug);
      setPublicArticles(category_articles);

      const [{ slug: articleSlug }] = category_articles;
      isNil(urlSlug) &&
        history.push(`/${articleSlug}?category=${currentCategorySlug}`);
    } catch (error) {
      logger.error(error);
    }
  };

  useEffect(() => {
    fetchPublicArticles();
  }, []);

  return (
    <div>
      {publicArticles.map(({ title, slug }) => (
        <Link key={slug} to={`/${slug}?category=${currentCategorySlug}`}>
          <div className="pb-2">
            <Typography
              className={
                slug === urlSlug && urlCategorySlug === currentCategorySlug
                  ? "neeto-ui-text-primary-800"
                  : "neeto-ui-text-gray-600"
              }
            >
              {title}
            </Typography>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default ListArticles;
