import React from "react";

import { Search } from "neetoicons";
import { Typography } from "neetoui";

const NavBar = ({
  organizationName,
  setShowSearchModal,
  isSearchBoxVisible,
}) => (
  <nav className="mb-0.5 bg-white">
    {isSearchBoxVisible && (
      <div
        className="border absolute m-5 flex space-x-1 border-black p-1 pr-8"
        style={{ cursor: "pointer" }}
        onClick={() => setShowSearchModal(true)}
      >
        <Search />
        <Typography className="mt-0.5 pr-1" style="body2">
          Search for articles here /
        </Typography>
        <Typography
          className="border	rounded-lg border-current bg-gray-200 p-1"
          style="nano"
        >
          Cmd
        </Typography>
        <Typography className="pt-1" style="nano">
          +
        </Typography>
        <Typography
          className="border rounded-lg border-black bg-gray-200 p-1 pl-2 pr-2"
          style="nano"
        >
          k
        </Typography>
      </div>
    )}
    <div className="flex h-16 justify-center pt-5">
      <Typography style="h4">{organizationName}</Typography>
    </div>
    <hr />
  </nav>
);

export default NavBar;
