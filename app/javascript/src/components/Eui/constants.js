import * as yup from "yup";

export const LOGIN_FORM_INITIAL_VALUES = {
  password: "",
};

export const LOGIN_FORM_VALIDATION_SCHEMA = yup.object().shape({
  password: yup.string().required("Password is required"),
});
