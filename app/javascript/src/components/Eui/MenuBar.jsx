import React from "react";

import { Accordion } from "neetoui";
import { MenuBar as Menu } from "neetoui/layouts";

import { useEuiState } from "contexts/eui";

import ListArticles from "./ListArticles";

const MenuBar = ({ publicCategories }) => {
  const { urlArticleSlug, urlCategorySlug } = useEuiState();

  const defaultKey = publicCategories
    .map(category => category.slug)
    .indexOf(urlCategorySlug);

  return (
    <Menu showMenu className="flex">
      <Accordion
        defaultActiveKey={defaultKey === -1 ? defaultKey + 1 : defaultKey}
      >
        {publicCategories.map(({ title, slug }) => (
          <Accordion.Item key={slug} title={title}>
            <div className="ml-4">
              <ListArticles
                currentCategorySlug={slug}
                urlCategorySlug={urlCategorySlug}
                urlSlug={urlArticleSlug}
              />
            </div>
          </Accordion.Item>
        ))}
      </Accordion>
    </Menu>
  );
};

export default MenuBar;
