export const keyDownPressed = (
  filteredArticles,
  arrowDownPressed,
  setCurrentArticlePosition
) => {
  if (filteredArticles.length && arrowDownPressed) {
    setCurrentArticlePosition(prevState =>
      prevState < filteredArticles.length - 1 ? prevState + 1 : prevState
    );
  }
};

export const keyUpPressed = (
  filteredArticles,
  arrowUpPressed,
  setCurrentArticlePosition
) => {
  if (filteredArticles.length && arrowUpPressed) {
    setCurrentArticlePosition(prevState =>
      prevState > 0 ? prevState - 1 : prevState
    );
  }
};

export const keyEnterPressed = (
  filteredArticles,
  enterPress,
  setSelectedArticle,
  currentArticlePosition
) => {
  if (filteredArticles.length && enterPress) {
    setSelectedArticle(filteredArticles[currentArticlePosition]);
  }
};

export const showArticle = (
  selectedArticle,
  history,
  setShowSearchModal,
  setSelectedArticle,
  setCurrentArticlePosition
) => {
  if (Object.keys(selectedArticle).length > 0) {
    const { article_slug, category_slug } = selectedArticle;
    history.push(`/${article_slug}?category=${category_slug}`);
    setShowSearchModal(false);
    setSelectedArticle([]);
    setCurrentArticlePosition(0);
  }
};
