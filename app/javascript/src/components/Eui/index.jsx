import React, { useEffect, useState } from "react";

import queryString from "query-string";
import { GlobalHotKeys } from "react-hotkeys";
import { useParams, useLocation } from "react-router-dom";

import publicCategoriesApi from "apis/Public/categories";
import { useEuiDispatch } from "contexts/eui";
import { getFromLocalStorage } from "utils/storage";

import MenuBar from "./MenuBar";
import NavBar from "./NavBar";
import NoDataPage from "./NoDataPage";
import SearchModal from "./SearchModal";
import ShowArticle from "./ShowArticle";

const Eui = () => {
  const [publicCategories, setPublicCategories] = useState([]);
  const [dataLoading, setDataLoading] = useState(false);
  const [showSearchModal, setShowSearchModal] = useState(false);

  const { slug } = useParams();
  const { search: searchCategory } = useLocation();
  const { category: categorySlug } = queryString.parse(searchCategory);
  const euiDispatch = useEuiDispatch();

  const organizationName = getFromLocalStorage("organizationName");

  const keyMap = {
    SNAP_LEFT: "command+k",
  };

  const fetchPublicCategories = async () => {
    try {
      const {
        data: { categories },
      } = await publicCategoriesApi.list();
      setPublicCategories(categories);
      setDataLoading(true);
    } catch (error) {
      logger.error(error);
    }
  };

  const handleKeyDown = () => {
    setShowSearchModal(true);
  };

  const globalHotKeysHandler = {
    SNAP_LEFT: handleKeyDown,
  };

  useEffect(() => {
    euiDispatch({
      type: "SET_EUI_SLUG",
      payload: { urlArticleSlug: slug, urlCategorySlug: categorySlug },
    });
  }, [euiDispatch, slug, categorySlug]);

  useEffect(() => {
    fetchPublicCategories();
  }, []);

  if (publicCategories.length < 1 && dataLoading) {
    return (
      <div className="h-screen w-full">
        <NoDataPage />
      </div>
    );
  }

  return (
    <GlobalHotKeys handlers={globalHotKeysHandler} keyMap={keyMap}>
      <NavBar
        isSearchBoxVisible
        organizationName={organizationName}
        setShowSearchModal={setShowSearchModal}
      />
      <div className="flex">
        <MenuBar publicCategories={publicCategories} />
        <ShowArticle urlArticleSlug={slug} urlCategorySlug={categorySlug} />
        <SearchModal
          setShowSearchModal={setShowSearchModal}
          showSearchModal={showSearchModal}
        />
      </div>
    </GlobalHotKeys>
  );
};

export default Eui;
