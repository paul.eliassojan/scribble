import React, { useEffect, useState } from "react";

import { Typography, Tag, PageLoader } from "neetoui";

import publicArticlesApi from "apis/Public/articles";
import PageNotFound from "components/Common/PageNotFound";
import { formatDate } from "components/Dashboard/utils";

const ShowArticle = ({ urlArticleSlug, urlCategorySlug }) => {
  const [notFound, setNotFound] = useState(false);
  const [article, setArticle] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const { title, body, category, updated_at: updatedAt } = article;

  const fetchPublicArticle = async () => {
    try {
      const {
        data: { article },
      } = await publicArticlesApi.show(urlArticleSlug, urlCategorySlug);
      setArticle(article);
      setNotFound(false);
      setIsLoading(false);
    } catch (error) {
      setNotFound(true);
      logger.error(error);
    }
  };

  useEffect(() => {
    if (urlArticleSlug !== undefined) {
      fetchPublicArticle(), setIsLoading(false);
    }
  }, [urlArticleSlug, urlCategorySlug]);

  if (isLoading) {
    return (
      <div className="h-screen w-full">
        <PageLoader />
      </div>
    );
  }

  if (notFound) {
    return (
      <div className="h-screen w-full">
        <PageNotFound />
      </div>
    );
  }

  return (
    <div>
      <Typography className="ml-10 mt-10" style="h1">
        {title}
      </Typography>
      <div className="ml-10 mt-5 flex">
        <Tag label={category} />
        <Typography className="neeto-ui-text-gray-500 ml-5" style="body2">
          {formatDate(updatedAt)}
        </Typography>
      </div>
      <div className="mx-12 mt-10">{body}</div>
    </div>
  );
};

export default ShowArticle;
