# frozen_string_literal: true

class Category < ApplicationRecord
  MAX_TITLE_LENGTH = 30
  VALID_TITLE_REGEX = /\A[a-zA-Z0-9]/.freeze

  belongs_to :user

  has_many :article_categories
  has_many :articles, through: :article_categories
  acts_as_list

  validates :slug, uniqueness: true
  validates :title, presence: true, length: { maximum: MAX_TITLE_LENGTH }, uniqueness: { case_sensitive: false },
    format: { with: VALID_TITLE_REGEX }
  validate :slug_not_changed

  before_create :set_slug

  private

    def set_slug
      title_slug = title.parameterize
      regex_pattern = "slug ~* ?"
      latest_category_slug = Category.where(
        regex_pattern,
        "#{title_slug}$|#{title_slug}-[0-9]+$"
      ).order("LENGTH(slug) DESC", slug: :desc).first&.slug
      slug_count = 0
      if latest_category_slug.present?
        slug_count = latest_category_slug.split("-").last.to_i
        only_one_slug_exists = slug_count == 0
        slug_count = 1 if only_one_slug_exists
      end
      slug_candidate = slug_count.positive? ? "#{title_slug}-#{slug_count + 1}" : title_slug
      self.slug = slug_candidate
    end

    def slug_not_changed
      if slug_changed? && self.persisted?
        errors.add(:slug, t("category.slug.immutable"))
      end
    end
end
