# frozen_string_literal: true

class Article < ApplicationRecord
  MAX_TITLE_LENGTH = 125
  VALID_TITLE_REGEX = /\A[a-zA-Z0-9]/.freeze
  PUBLIC_SEARCH_LIMIT = 3

  enum status: { draft: "draft", published: "published" }, _default: :draft

  belongs_to :user

  has_many :schedulers, dependent: :destroy
  has_many :article_categories, dependent: :destroy
  has_many :categories, through: :article_categories, dependent: :destroy
  has_many :visits, dependent: :destroy

  validates :article_categories, presence: true
  validates :title, presence: true, length: { maximum: MAX_TITLE_LENGTH }, format: { with: VALID_TITLE_REGEX }
  validates :slug, uniqueness: true, if: -> { status == :published }, presence: true
  validates :body, presence: true
  validate :slug_not_changed
  validate :has_at_least_one_category

  has_paper_trail on: %i[create update]

  before_save :set_slug
  before_create :update_last_published_at
  before_update :update_last_published_at

  private

    def set_slug
      if self.published? && slug.nil?
        title_slug = title.parameterize
        regex_pattern = "slug ~* ?"
        latest_article_slug = Article.where(
          regex_pattern,
          "#{title_slug}$|#{title_slug}-[0-9]+$"
        ).order("LENGTH(slug) DESC", slug: :desc).first&.slug
        slug_count = 0
        if latest_article_slug.present?
          slug_count = latest_article_slug.split("-").last.to_i
          only_one_slug_exists = slug_count == 0
          slug_count = 1 if only_one_slug_exists
        end
        slug_candidate = slug_count.positive? ? "#{title_slug}-#{slug_count + 1}" : title_slug
        self.slug = slug_candidate
      end
    end

    def slug_not_changed
      if slug_changed? && self.persisted?
        errors.add(:slug, t("article.slug.immutable"))
      end
    end

    def has_at_least_one_category
      errors.add(:categories, t("article.categories.is_not_empty")) if categories.empty?
    end

    def update_last_published_at
      if self.published?
        self.last_published_at = self.updated_at
      end
    end
end
