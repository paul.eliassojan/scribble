# frozen_string_literal: true

class ArticleCategory < ApplicationRecord
  belongs_to :article
  belongs_to :category

  counter_culture :category, column_name: :articles_count
end
