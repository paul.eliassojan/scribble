# frozen_string_literal: true

class Scheduler < ApplicationRecord
  belongs_to :article

  validates :scheduling_date_time, presence: true
  validates :status, presence: true, inclusion: { in: %w(draft published), message: :bad_status }
  validate :scheduling_date_time_should_not_fall_behind_current_date_time
  validate :second_schedule_date_time_should_not_fall_behind_first_schedule
  validate :schedule_already_exists
  validate :scheduling_status_and_article_status_is_same

  before_validation :convert_to_utc

  private

    def convert_to_utc
      self.scheduling_date_time = Time.parse(scheduling_date_time.strftime("%d/%m/%Y %H")).utc
    end

    def scheduling_date_time_should_not_fall_behind_current_date_time
      errors.add(:scheduling, t("article.schedule.not_possible")) if
      scheduling_date_time <= Time.zone.now
    end

    def get_schedules
      article.schedulers.where("scheduling_date_time > ?", Time.zone.now)
    end

    def schedule_already_exists
      check_artilce_status_present_in_schedule = get_schedules.where(status: status)
      if get_schedules.present? && check_artilce_status_present_in_schedule.present?
        errors.add(:base, t("article.schedule.already_exists", entity: "Article"))
      end
    end

    def scheduling_status_and_article_status_is_same
      if !get_schedules.present?
        errors.add(:scheduling, t("article.schedule.not_possible")) if article.status == status
      else
        check_artilce_status_present_in_schedule = get_schedules.pluck(:status).include?(status)
        errors.add(
          :scheduling,
          t("article.schedule.not_possible")) if check_artilce_status_present_in_schedule
      end
    end

    def second_schedule_date_time_should_not_fall_behind_first_schedule
      if get_schedules.present?
        errors.add(:base, t("article.schedule.invalid_time")) if
        scheduling_date_time <= get_schedules.pluck(:scheduling_date_time).first
      end
    end
end
