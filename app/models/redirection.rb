# frozen_string_literal: true

class Redirection < ApplicationRecord
  VALID_REDIRECTION_REGEX = /\A[a-zA-Z0-9]/.freeze

  belongs_to :user

  validates :from_path, presence: true, uniqueness: true, format: { with: VALID_REDIRECTION_REGEX }
  validates :to_path, presence: true, format: { with: VALID_REDIRECTION_REGEX }
  validate :from_path_and_to_path_shouldnt_be_same
  validate :check_cycle

  private

    def from_path_and_to_path_shouldnt_be_same
      return nil if to_path.empty? || from_path.empty?
      return errors.add(:base, t("redirection.cannot_create", entity: "Redirection")) if
      to_path.eql? from_path
    end

    def check_cycle
      all_from_paths = Redirection.pluck(:from_path)
      all_to_paths = Redirection.pluck(:to_path)
      check_from_path = to_path

      while all_from_paths.include?(check_from_path) do
        possible_from_path = Redirection.find_by(from_path: check_from_path).to_path
        check_from_path = possible_from_path

        return errors.add(:base, t("redirection.cannot_create", entity: "Redirection")) if
        possible_from_path == from_path
      end
    end
end
