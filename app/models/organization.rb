# frozen_string_literal: true

class Organization < ApplicationRecord
  MAX_NAME_LENGTH = 35
  MIN_PASSWORD_LENGTH = 6
  VALID_PASSWORD_REGEX = /\A^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$\z/i.freeze

  has_one :user
  has_secure_password(validations: false)
  has_secure_token :authentication_token

  validates :name, presence: true, length: { maximum: MAX_NAME_LENGTH }
  validates :password, length: { minimum: MIN_PASSWORD_LENGTH }, format: { with: VALID_PASSWORD_REGEX },
    if: -> { password.present? }
  validates :password_protected, inclusion: { in: [ true, false ] }

  before_save :destroy_password

  private

    def destroy_password
      password = nil if !password_protected
    end
end
