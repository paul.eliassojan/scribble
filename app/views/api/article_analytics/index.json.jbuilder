# frozen_string_literal: true

json.articles @published_articles do |published_article|
  json.extract! published_article,
    :id,
    :title,
    :updated_at
  json.categories published_article.categories do |category|
    json.partial! "api/categories/category", category: category
  end
  json.article_visits published_article.visits.count
end
json.articles_size @published_articles_size
