# frozen_string_literal: true

json.article_visits @published_article_visits do |published_article_visit|
  json.extract! published_article_visit,
    :date,
    :visits
end
