# frozen_string_literal: true

json.articles @articles_per_page_and_limit do |article|
  json.partial! "api/articles/article", article: article
  json.partial! "api/users/user", user: article.user
  json.categories article.categories do |category|
    json.partial! "api/categories/category", category: category
  end
end

json.articles_count do
  json.all Article.all.count
  json.published Article.where(status: :published).count
  json.draft Article.where(status: :draft).count
end
json.filtered_articles_size @articles.size
