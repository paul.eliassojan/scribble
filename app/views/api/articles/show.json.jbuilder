# frozen_string_literal: true

json.partial! "api/articles/article", article: @article
json.partial! "api/users/user", user: @article.user
json.categories @article.categories do |category|
  json.partial! "api/categories/category", category: category
end
if @article.restore_time != nil
  json.restore_version_time @article.restore_time
end
