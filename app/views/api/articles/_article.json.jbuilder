# frozen_string_literal: true

json.extract! article,
  :id,
  :title,
  :body,
  :status,
  :updated_at,
  :last_published_at,
  :is_scheduled
