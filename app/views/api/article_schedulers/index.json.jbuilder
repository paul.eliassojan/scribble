# frozen_string_literal: true

json.article_schedules @article_schedules do |article_schedule|
  json.extract! article_schedule,
    :id,
    :scheduling_date_time,
    :status
end
