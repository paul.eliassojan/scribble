# frozen_string_literal: true

json.categories @categories do |category|
  if category.articles.published.length > 0
    json.extract! category,
      :title,
      :slug
  end
end
