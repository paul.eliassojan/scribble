# frozen_string_literal: true

json.category_articles @category_published_articles do |category_article|
  json.extract! category_article,
    :title,
    :slug
end
