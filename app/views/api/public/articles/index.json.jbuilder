# frozen_string_literal: true

json.articles @filtered_public_articles do |article|
  json.array! article.categories do |category|
    json.article_title article.title
    json.article_slug article.slug
    json.category_title category.title
    json.category_slug category.slug
  end
end
