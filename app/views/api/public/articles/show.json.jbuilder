# frozen_string_literal: true

json.article do
  json.extract! @article,
    :title,
    :body,
    :updated_at,
    :slug
  json.category @article_category.title
end
