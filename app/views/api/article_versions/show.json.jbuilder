# frozen_string_literal: true

json.extract! @article_version,
  :id
json.extract! @article_version.reify,
  :title,
  :body
json.categories @article_version.reify.categories do |category|
  json.partial! "api/categories/category", category: category
end
