# frozen_string_literal: true

json.article_versions @article_versions do |article_version|
  if !article_version.object.nil?
    json.extract! article_version,
      :id,
      :created_at,
      :event
    json.extract! article_version.reify,
      :status,
      :is_scheduled
    if article_version.reify.restore_time != nil
      json.restore_version_time article_version.reify.restore_time
    end
  end
end
