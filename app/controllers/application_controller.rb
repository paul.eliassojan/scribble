# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include ApiRescuable
  include ApiResponders
  before_action :set_paper_trail_whodunnit

  private

    def current_user
      @_current_user ||= current_organization.user
    end

    def current_organization
      @_current_organization ||= Organization.first
    end

    def require_login
      if current_organization.password_protected?

        return respond_with_error(t("session.could_not_auth")) unless
        !session[:auth].nil?
      end
    end
end
