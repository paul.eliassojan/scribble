# frozen_string_literal: true

class HomeController < ApplicationController
  before_action :redirect_url

  def index
    render
  end

  private

    def redirect_url
      from_path = request.fullpath[1..]

      redirection = Redirection.find_by(from_path: from_path)

      return redirect_to "/#{redirection.to_path}", status: 301 unless redirection.nil?
    end
end
