# frozen_string_literal: true

class Api::ArticleSchedulersController < ApplicationController
  before_action :load_article!, except: %i[new edit]
  before_action :load_article_schedule!, only: :destroy

  def index
    @article_schedules = @article.schedulers.where("scheduling_date_time > ?", Time.zone.now)
  end

  def create
    @article.schedulers.create!(scheduler_params)
    respond_with_success(t("successfully_scheduled", entity: "Article"))
  end

  def destroy
    @schedule.destroy! if @article.status == @schedule.status
    @article.schedulers.delete_all if @article.status != @schedule.status
    respond_with_success(t("successfully_deleted", entity: "Schedule"))
  end

  private

    def scheduler_params
      params.require(:scheduler).permit(:scheduling_date_time, :status)
    end

    def load_article!
      @article = current_user.articles.find(params[:article_id])
    end

    def load_article_schedule!
      @schedule = @article.schedulers.find(params[:id])
    end
end
