# frozen_string_literal: true

class Api::CategoriesController < ApplicationController
  before_action :load_category!, only: %i[update destroy]

  def index
    @categories = current_user.categories.all.order(position: :asc)
  end

  def create
    current_user.categories.create!(category_params)
    respond_with_success(t("successfully_created", entity: "Category"))
  end

  def update
    @category.update!(category_params)
    respond_with_success(t("successfully_updated", entity: "Category")) unless params.key?(:quiet)
  end

  def destroy
    response = CategoryDeletionService.new(@category, params[:category_id], current_user).process
    respond_with_error(t("category.cannot_delete", entity: "Category")) if !response.success?
    respond_with_success(t("successfully_deleted", entity: "Category")) if response.success?
  end

  private

    def category_params
      params.require(:category).permit(:title, :position)
    end

    def load_category!
      @category = current_user.categories.find(params[:id])
    end
end
