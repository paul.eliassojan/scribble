# frozen_string_literal: true

class Api::ArticlesController < ApplicationController
  before_action :load_article!, only: %i[show update destroy]

  def index
    @articles = ArticlesFilterService.new(
      params[:category], params[:status], params[:search],
      current_user).process.order(updated_at: :desc)
    @articles_per_page_and_limit = @articles.page(params[:page]).per(params[:limit])
  end

  def create
    article = current_user.articles.create!(article_params)
    article.schedulers.create!(scheduler_params) if params.key?(:quiet)
    respond_with_success(t("successfully_created", entity: "Article"))
  end

  def show
    render
  end

  def update
    article_schedule_present
    @article.restore_time = nil
    @article.is_scheduled = false
    @article.update!(article_params)
    respond_with_success(t("successfully_updated", entity: "Article"))
  end

  def destroy
    @article.destroy!
    respond_with_success(t("successfully_deleted", entity: "Article"))
  end

  private

    def article_params
      params.require(:article).permit(:title, :body, :status, :restore_time, :is_scheduled, category_ids: [])
    end

    def scheduler_params
      params.require(:scheduler).permit(:scheduling_date_time, :status)
    end

    def load_article!
      @article = current_user.articles.find(params[:id])
    end

    def article_schedule_present
      schedule = @article.schedulers.where(
        "scheduling_date_time > ?",
        Time.zone.now).where(status: article_params[:status])
      @article.schedulers.destroy(schedule) if schedule.present? && @article.status != article_params[:status]
    end
end
