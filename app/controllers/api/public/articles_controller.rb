# frozen_string_literal: true

class Api::Public::ArticlesController < ApplicationController
  before_action :require_login, only: %i[index show]
  before_action :load_article!, only: :show
  before_action :filter_public_articles, only: :index

  def index
    @filtered_public_articles
  end

  def show
    @article_category = @article.categories.find_by!(slug: params[:category])
    @article.visits.create!
  end

  private

    def load_article!
      @article = current_user.articles.published.find_by!(slug: params[:slug])
    end

    def filter_public_articles
      if params[:search].present?
        search_data = params[:search].downcase
      end
      @filtered_public_articles = current_user.articles.published.where(
        "lower(title) LIKE ?",
        "%#{search_data}%").limit(Article::PUBLIC_SEARCH_LIMIT)
    end
end
