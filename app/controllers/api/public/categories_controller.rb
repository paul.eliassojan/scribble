# frozen_string_literal: true

class Api::Public::CategoriesController < ApplicationController
  before_action :require_login, only: %i[index show]

  before_action :load_category!, only: :show

  def index
    @categories = current_user.categories.all.order(position: :asc)
  end

  def show
    @category_published_articles = @category.articles.select { |article| article.published? }
  end

  private

    def load_category!
      @category = current_user.categories.find_by!(slug: params[:slug])
  end
end
