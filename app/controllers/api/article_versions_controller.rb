# frozen_string_literal: true

class Api::ArticleVersionsController < ApplicationController
  before_action :load_article!, except: %i[new edit create]
  before_action :load_article_version!, only: %i[show update]

  def index
    @article_versions = @article.versions.reorder(created_at: :desc)
  end

  def show
    render
  end

  def update
    article_schedule_present
    @article_version.reify.update!(
      status: "draft", slug: @article.slug, restore_time: @article_version.created_at,
      last_published_at: @article.last_published_at, is_scheduled: false)
    respond_with_success(t("article.version.restored", entity: "Article"))
  end

  private

    def load_article!
      @article = current_user.articles.find(params[:article_id])
    end

    def load_article_version!
      @article_version = @article.versions.find_by!(id: params[:id])
    end

    def article_schedule_present
      schedule = @article.schedulers.where(
        "scheduling_date_time > ?",
        Time.zone.now).where(status: "draft")
      @article.schedulers.destroy(schedule) if schedule.present? && @article.status != "draft"
    end
end
