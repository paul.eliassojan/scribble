# frozen_string_literal: true

class Api::RedirectionsController < ApplicationController
  before_action :load_redirection!, only: %i[update destroy]

  def index
    @redirections = current_user.redirections.all
  end

  def create
    current_user.redirections.create!(redirection_param)
    respond_with_success(t("successfully_created", entity: "Redirection"))
  end

  def update
    @redirection.update!(redirection_param)
    respond_with_success(t("successfully_updated", entity: "Redirection"))
  end

  def destroy
    @redirection.destroy!
    respond_with_success(t("successfully_deleted", entity: "Redirection"))
  end

  private

    def redirection_param
      params.require(:redirection).permit(:from_path, :to_path)
    end

    def load_redirection!
      @redirection = current_user.redirections.find(params[:id])
    end
end
