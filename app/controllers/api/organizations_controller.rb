# frozen_string_literal: true

class Api::OrganizationsController < ApplicationController
  def show
    @organization = current_organization
  end

  def update
    session.delete(:auth)
    current_organization.update!(organization_params)
    respond_with_success(t("successfully_updated", entity: "Organization"))
  end

  private

    def organization_params
      params.require(:organization).permit(:name, :password, :password_protected)
    end
end
