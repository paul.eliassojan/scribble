# frozen_string_literal: true

class Api::SessionsController < ApplicationController
  def create
    organization = Organization.find_by!(name: login_params[:name])
    if organization && organization.authenticate(login_params[:password])
      session[:auth] = organization.authentication_token
    else
      respond_with_error(t("session.incorrect_credentials"), :unauthorized)
    end
  end

  private

    def login_params
      params.require(:session).permit(:name, :password)
    end
end
