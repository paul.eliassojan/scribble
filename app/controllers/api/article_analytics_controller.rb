# frozen_string_literal: true

class Api::ArticleAnalyticsController < ApplicationController
  before_action :load_article!, only: :show
  before_action :published_articles_size, only: :index

  def index
    @published_articles = current_user.articles.published.page(params[:page]).per(params[:limit])
  end

  def show
    @published_article_visits = @article.visits.group("DATE(created_at)").select(
      "DATE(created_at),COUNT(created_at) as visits")
  end

  private

    def load_article!
      @article = current_user.articles.find(params[:id])
    end

    def published_articles_size
      @published_articles_size = current_user.articles.published.size
    end
end
