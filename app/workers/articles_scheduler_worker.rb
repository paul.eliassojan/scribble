# frozen_string_literal: true

class ArticlesSchedulerWorker
  include Sidekiq::Worker

  def perform
    articles_scheduler_service = ArticlesSchedulerService.new
    articles_scheduler_service.process
  end
end
