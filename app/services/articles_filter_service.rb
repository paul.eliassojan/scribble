# frozen_string_literal: true

class ArticlesFilterService
  attr_reader :category, :status, :search, :user

  def initialize(category, status, search, user)
    @category = category
    @status = status
    @search = search
    @user = user
  end

  def process
    filter_articles
  end

  private

    def filter_articles
      categories_data = {}
      status_data = {}

      if category.present? && category != "undefined"
        category_name = category.split(",")
        categories_data = { categories: { title: [category_name] } }
      end
      if status.present? && status != "undefined"
        status_data = { status: status }
      end
      if search.present? && search != "undefined"
        search_data = search.downcase
      end

      user.articles.includes(
        :categories,
        :user).where(categories_data).where(status_data).where("lower(articles.title) LIKE ?", "%#{search_data}%")
    end
end
