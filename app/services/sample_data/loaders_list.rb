# frozen_string_literal: true

module SampleData
  class LoadersList
    def process!
      SampleData::BuildOrganization.new.process!
      SampleData::BuildUser.new.process!
      SampleData::PopulateSampleCategories.new.process!
      SampleData::PopulateSampleRedirection.new.process!
      SampleData::PopulateSampleArticles.new.process!
    end
  end
end
