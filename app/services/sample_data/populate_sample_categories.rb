# frozen_string_literal: true

module SampleData
  class PopulateSampleCategories
    attr_accessor :organization, :user

    def initialize
      @organization = Organization.find_by!(name: "Spinkart")
      @user = organization.user
    end

    def process!
      populate_categories!
    end

    private

      def populate_categories!
        4.times do
          params = {
            title: Faker::Lorem.unique.sentence[0..20]
          }
          create_category!(params)
        end
      end

      def create_category!(options = {})
        category_attributes = {
          user_id: user.id
        }
        attributes = category_attributes.merge options
        user.categories.create! attributes
      end
  end
end
