# frozen_string_literal: true

module SampleData
  class PopulateSampleArticles
    attr_accessor :organization, :user, :categories, :status

    def initialize
      @organization = Organization.find_by!(name: "Spinkart")
      @user = organization.user
      @categories = user.categories.pluck(:id)
      @status = ["draft", "published"]
    end

    def process!
      populate_published_articles!
      populate_draft_articles!
      populate_article_versions!
    end

    private

      def populate_draft_articles!
        6.times do
          params = {
            title: Faker::Lorem.sentence[0..49],
            body: Faker::Lorem.paragraph(sentence_count: rand(10..100)),
            category_ids: categories.sample,
            status: :draft
          }
          create_article!(params)
        end
      end

      def populate_published_articles!
        7.times do
          params = {
            title: Faker::Lorem.sentence[0..49],
            body: Faker::Lorem.paragraph(sentence_count: rand(10..100)),
            category_ids: categories.sample(2),
            status: :published
          }
          create_article!(params)
        end
      end

      def create_article!(options = {})
        article_attributes = {
          user_id: user.id
        }
        attributes = article_attributes.merge options
        user.articles.create! attributes
      end

      def populate_article_versions!
        20.times do
          params = {
            title: Faker::Lorem.sentence[0..49],
            body: Faker::Lorem.paragraph(sentence_count: rand(10..100)),
            status: status.sample()
          }
          create_article_version!(params)
        end
      end

      def create_article_version!(options = {})
        article_attributes = {
          user_id: user.id
        }
        attributes = article_attributes.merge options
        user.articles.find(user.articles.pluck(:id).sample).update! attributes
      end
  end
end
