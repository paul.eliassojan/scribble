# frozen_string_literal: true

module SampleData
  class PopulateSampleRedirection
    attr_accessor :organization, :user

    def initialize
      @organization = Organization.find_by!(name: "Spinkart")
      @user = organization.user
    end

    def process!
      create_redirection!
    end

    private

      def create_redirection!(options = {})
        redirection_attributes = {
          from_path: Faker::Internet.unique.domain_word,
          to_path: Faker::Internet.unique.domain_word,
          user_id: user.id
        }
        attributes = redirection_attributes.merge options
        user.redirections.create! attributes
      end
  end
end
