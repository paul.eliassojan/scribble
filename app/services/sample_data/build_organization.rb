# frozen_string_literal: true

module SampleData
  class BuildOrganization
    def process!
      create_organization!
    end

    private

      def create_organization!(options = {})
        organization_attributes = {
          name: "Spinkart"
        }
        @organization = Organization.new(organization_attributes)
        @organization.save!(validations: false)
      end
  end
end
