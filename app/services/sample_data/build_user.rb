# frozen_string_literal: true

module SampleData
  class BuildUser
    attr_accessor :organization

    def initialize
      @organization = Organization.find_by!(name: "Spinkart")
    end

    def process!
      create_user!
    end

    private

      def create_user!(options = {})
        user_attributes = {
          name: "Oliver Smith",
          email: "oliver@example.com",
          organization_id: organization.id
        }
        attributes = user_attributes.merge options
        User.create! attributes
      end
  end
end
