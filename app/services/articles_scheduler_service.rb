# frozen_string_literal: true

class ArticlesSchedulerService
  attr_reader :articles_to_schedule

  def initialize
    @articles_to_schedule = get_articles_to_schedule
  end

  def process
    schedule_articles
  end

  private

    def get_articles_to_schedule
      Scheduler.where(
        scheduling_date_time: Time.zone.now.strftime("%a, %d %b %Y %H:%M"))
    end

    def schedule_articles
      articles_to_schedule.each do |article_to_schedule|
        current_article = Article.find_by(id: article_to_schedule.article_id)
        next if current_article.nil?

        current_article.update!(status: article_to_schedule.status, is_scheduled: true)
      end
    end
end
