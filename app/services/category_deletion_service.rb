# frozen_string_literal: true

class CategoryDeletionService
  attr_reader :category, :new_category_id, :user

  def initialize(category, new_category_id, user)
    @category = category
    @new_category_id = new_category_id
    @user = user
  end

  def process
    destroy_category_after_assigning_articles_to_new_category
  end

  private

    def destroy_category_after_assigning_articles_to_new_category
      if user.categories.count == 1 && category.title.downcase != "general"
        category_id = default_category().id
        new_category = user.categories.find_by!(id: category_id)

      elsif category.title.downcase == "general"
        return OpenStruct.new({ success?: false })

      else
        new_category = user.categories.find_by!(id: new_category_id)
      end

      if category.title.downcase != "general"
        category.articles.each do |article|
          article.article_categories.delete_by(category_id: category.id) if
          category_to_be_destroyed_article_exists_in_new_category(new_category, article.id)
          article.article_categories.where(category_id: category.id).update(category: new_category)
        end
        category.destroy!
      end
      OpenStruct.new({ success?: true })
    end

    def default_category
      user.categories.create!(title: "General")
    end

    def category_to_be_destroyed_article_exists_in_new_category(new_category, article_id)
      new_category.articles.exists?(id: article_id)
    end
end
