# frozen_string_literal: true

class ArticlesSchedulerWorkerTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
    @scheduler = create(:scheduler, article: @article)

    travel_to DateTime.parse(@scheduler.scheduling_date_time.to_s)
  end

  def test_articles_scheduler_worker_are_getting_processed
    ArticlesSchedulerWorker.perform_async
    @article.reload
    assert_equal @article.status, @scheduler.status
  end
end
