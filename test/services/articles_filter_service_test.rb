# frozen_string_literal: true

require "test_helper"

class ArticlesFilterServiceTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
  end

  def test_should_filter_articles_based_on_article_status
    create(:article, categories: [@category], user: @user, status: :published)
    category_title = ""
    article_status = "draft"
    search_query = ""
    filtered_articles = filter_articles(category_title, article_status, search_query, @user).process

    check_article_is_present = filtered_articles.find(@article.id)
    assert_equal @article.id, check_article_is_present.id
  end

  def test_should_filter_articles_based_on_search_query
    new_article = create(:article, categories: [@category], user: @user, status: :published)
    category_title = ""
    article_status = ""
    search_query = new_article.title.downcase

    filtered_articles_from_service = filter_articles(category_title, article_status, search_query, @user).process
    filtered_articles = @organization.user.articles.where("lower(title) LIKE ?", "%#{@article.title.downcase}%")

    assert_equal filtered_articles.size, filtered_articles_from_service.size
  end

  def test_should_filter_articles_based_on_categories
    new_category_two = create(:category, user: @user)
    new_article_two = create(:article, categories: [new_category_two], user: @user)
    category_title = [@category.title, new_category_two.title]
    article_status = ""
    search_query = ""

    filtered_articles_from_service = filter_articles(category_title, article_status, search_query, @user).process
    filtered_articles = @organization.user.articles.includes(:categories).where(
      {
        categories: {
          title: [@category.title,
          new_category_two.title]
        }
      })
    assert_equal filtered_articles.size, filtered_articles_from_service.size
  end

  def test_should_filter_articles_based_on_status_categories_and_search_query
    new_category_two = create(:category, user: @user)
    new_article_two = create(:article, categories: [new_category_two], user: @user, status: :published)
    category_title = [@category.title, new_category_two.title]
    article_status = "published"
    search_query = new_article_two.title

    filtered_articles_from_service = filter_articles(
      category_title, article_status, search_query,
      @user).process

    filtered_articles = @organization.user.articles.includes(:categories).where(status: :published).where(
      {
        categories: {
          title: [new_category_two.title]
        }
      }).where(
        "lower(articles.title) LIKE ?", "%#{new_article_two.title.downcase}%")

    assert_equal filtered_articles.size, filtered_articles_from_service.size
  end

  private

    def filter_articles(category, status, search, user)
      ArticlesFilterService.new(category, status, search, user)
    end
end
