# frozen_string_literal: true

require "test_helper"

class CategoryDeletionServiceTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
  end

  def test_category_deletion_service_assigns_articles_to_new_category
    new_category = create(:category, title: "react", user: @user)
    destroy_category(@category, new_category.id, @user).process
    new_category.reload

    article_category = @organization.user.categories.find_by!(title: new_category.title)
    assert_equal new_category.articles.size, article_category.articles.size
  end

  def test_category_deletion_service_assign_articles_to_general_category_if_last_category_got_destroyed
    destroy_category(@category, @category.id, @user).process
    category = @organization.user.categories.find_by(title: "General")
    assert_equal category.valid?, true
  end

  def test_category_deletion_service_should_destroy_category_after_assigning_articles_to_new_category
    new_category = create(:category, title: "react", user: @user)
    assert_difference "@organization.user.categories.count", -1 do
      destroy_category(@category, new_category.id, @user).process
      new_category.reload
    end
  end

  def test_category_deletion_service_should_not_destroy_general_category
    new_category = create(:category, title: "general", user: @user)
    response = destroy_category(new_category, @category.id, @user).process

    assert_equal response.success?, false
  end

  def test_category_deletion_service_should_avoid_category_duplication_if_article_already_exist_in_new_category
    category_to_delete = create(:category, title: "react", user: @user)
    article = create(:article, categories: [@category, category_to_delete], user: @user)

    destroy_category(category_to_delete, @category.id, @user).process
    article.reload
    assert_equal article.categories.size, @organization.user.categories.count
  end

  private

    def destroy_category(category, new_category_id, user)
      CategoryDeletionService.new(category, new_category_id, user)
    end
end
