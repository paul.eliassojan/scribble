# frozen_string_literal: true

require "test_helper"
require "support/sidekiq_helper"

class ArticlesSchedulerServiceTest < ActiveSupport::TestCase
  include SidekiqHelper

  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
    @scheduler = create(:scheduler, article: @article)
  end

  def test_articles_scheduler_service_will_schedule_articles
    travel_to DateTime.parse(@scheduler.scheduling_date_time.to_s)

    articles_scheduler_service.process
    @article.reload
    assert_equal @article.status, @scheduler.status
  end

  def test_articles_scheduler_service_will_only_schedule_articles_when_current_time_matches_with_scheduled_time
    new_article_one = create(:article, categories: [@category], user: @user)
    new_schedule_one = create(:scheduler, article: new_article_one)
    new_article_two = create(:article, categories: [@category], user: @user)
    new_schedule_two = create(:scheduler, article: new_article_two)

    travel_to DateTime.parse(new_schedule_one.scheduling_date_time.to_s)
    articles_scheduler_service.process
    new_article_one.reload
    new_article_two.reload

    assert_equal new_article_one.status, new_schedule_one.status
    assert_not_same new_article_two.status, new_schedule_two.status
  end

  def test_articles_scheduler_service_will_update_is_schedule_to_true_on_successful_schedule
    new_article_one = create(:article, categories: [@category], user: @user)
    new_schedule_one = create(:scheduler, article: new_article_one)

    travel_to DateTime.parse(new_schedule_one.scheduling_date_time.to_s)
    articles_scheduler_service.process
    new_article_one.reload
    assert new_article_one.is_scheduled
  end

  private

    def articles_scheduler_service
      ArticlesSchedulerService.new
    end
end
