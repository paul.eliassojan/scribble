# frozen_string_literal: true

FactoryBot.define do
  factory :redirection do
    user
    from_path { Faker::Internet.unique.domain_word }
    to_path { Faker::Internet.unique.domain_word }
  end
end
