# frozen_string_literal: true

FactoryBot.define do
  factory :article do
    user
    title { Faker::Lorem.sentence[0..49] }
    body { Faker::Lorem.paragraph }
    status { :draft }

    trait :article_with_categories do
      after(:build) do |article|
        article.categories << create(:category)
      end
    end
  end
end
