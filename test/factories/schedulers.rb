# frozen_string_literal: true

FactoryBot.define do
  factory :scheduler do
    article
    scheduling_date_time { Faker::Time.between(from: DateTime.now, to: 1.year.from_now, format: "%d/%m/%Y %H:00") }
    status { "published" }
  end
end
