# frozen_string_literal: true

FactoryBot.define do
  factory :category do
    user
    title { Faker::Lorem.sentence[0..20] }
  end
end
