# frozen_string_literal: true

FactoryBot.define do
  factory :organization do
    name { Faker::Lorem.sentence[0..34] }
    password { "rails4567" }
    password_protected { false }
  end
end
