# frozen_string_literal: true

require "test_helper"

class ArticleCategoryTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @article = create(:article, :article_with_categories, user: @user)
  end

  def test_article_id_and_category_id_get_added_to_article_category_table_on_creating_new_article
    article_id = @article.id
    category_id = @article.categories.first.id
    article_category = @organization.user.articles.first.article_categories.first

    assert_equal article_id, article_category.article_id
    assert_equal category_id, article_category.category_id
  end

  def test_article_category_entry_should_be_deleted_when_deleting_corresponding_article
    assert_difference "@article.article_categories.count", -1 do
      @article.destroy
    end
  end

  def test_should_not_be_valid_if_invalid_article_id_and_category_id_added_to_article_category
    article_category = ArticleCategory.new(article_id: "1234", category_id: "4567")
    assert_not article_category.valid?

    assert_includes article_category.errors.full_messages.to_sentence, t("must_exists", entity: "Article"),
      t("must_exists", entity: "Category")
  end

  def test_article_category_should_not_be_valid_without_corresponding_article_id_and_category_id
    @article.article_categories = []
    assert_not @article.save
    assert_equal t("article_categories.not_blank", entity: "Article categories"),
      @article.errors.full_messages.to_sentence
  end
end
