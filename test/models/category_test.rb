# frozen_string_literal: true

require "test_helper"

class CategoryTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
  end

  def test_category_should_not_be_valid_without_title
    @category.title = ""
    assert_not @category.valid?
    assert_includes @category.errors.full_messages.to_sentence, t("category.invalid", entity: "Category"),
      t("category.not_blank", entity: "Category")
  end

  def test_category_title_should_not_exceed_maximum_length
    @category.title = "a" * (Category::MAX_TITLE_LENGTH + 1)
    assert_not @category.valid?
  end

  def test_category_title_should_start_with_alphanumeric_character
    @category.title = "/"
    assert_not @category.valid?
    assert_includes t("category.invalid", entity: "Category"), @category.errors.full_messages.to_sentence
  end

  def test_category_title_must_be_unique
    new_title = @category.title
    category = build(:category, title: new_title, user: @user)
    assert_not category.valid?
    assert_equal t("category.exists", entity: "Category"), category.errors.full_messages.to_sentence
  end

  def test_category_can_include_many_articles
    assert_difference ["Category.first.articles.size"], 1 do
      create(:article, categories: [@category], user: @user)
    end
  end

  def test_should_increment_articles_count_if_new_article_is_assigned_to_that_category
    article = create(:article, categories: [@category], user: @user)
    article.reload
    category_articles_count = @organization.user.categories.first.articles_count
    assert_equal @organization.user.articles.count, category_articles_count
  end

  def test_should_decrement_articles_count_if_article_belongs_to_that_category_get_destroyed
    assert_difference "Category.first.articles_count", -1 do
      @article.destroy
    end
  end

  def test_should_not_create_category_with_case_insensitive
    new_category_one = create(:category, title: "test", user: @user)
    new_category_two = build(:category, title: "TEst", user: @user)

    assert_not new_category_two.valid?
    assert_equal t("already_exists", entity: "Category"), new_category_two.errors.full_messages.to_sentence
  end

  def test_category_slug_is_parameterized_title
    title = @category.title
    @category.save!
    assert_equal title.parameterize, @category.slug
  end

  def test_incremental_slug_generation_for_categories_with_duplicate_two_worded_titles
    first_category = @organization.user.categories.create!(title: "test category 1", user: @user)
    second_category = @organization.user.categories.create!(title: "test category 2", user: @user)

    assert_equal "test-category-1", first_category.slug
    assert_equal "test-category-2", second_category.slug
  end

  def test_incremental_slug_generation_for_categories_with_duplicate_hyphenated_titles
    first_category = @organization.user.categories.create!(title: "test-category", user: @user)
    second_category = @organization.user.categories.create!(title: "test-category-2", user: @user)

    assert_equal "test-category", first_category.slug
    assert_equal "test-category-2", second_category.slug
  end

  def test_slug_generation_for_categories_having_titles_one_being_prefix_of_the_other
    first_category = @organization.user.categories.create!(title: "fishing", user: @user)
    second_category = @organization.user.categories.create!(title: "fish", user: @user)

    assert_equal "fishing", first_category.slug
    assert_equal "fish", second_category.slug
  end

  def test_error_raised_for_duplicate_slug
    another_test_category = @organization.user.categories.create!(title: "another test category", user: @user)

    assert_raises ActiveRecord::RecordInvalid do
      another_test_category.update!(slug: @category.slug)
    end

    error_msg = another_test_category.errors.full_messages.to_sentence
    assert_match t("category.slug.immutable"), error_msg
  end

  def test_updating_title_does_not_update_slug
    assert_no_changes -> { @category.reload.slug } do
      updated_category_title = "updated category title"
      @category.update!(title: updated_category_title)
      assert_equal updated_category_title, @category.title
    end
  end

  def test_slug_suffix_is_maximum_slug_count_plus_one_if_two_or_more_slugs_already_exist
    title = "general"
    first_category = create(:category, user: @user, title: title)
    first_category.update!(title: "general-1")
    second_category = create(:category, user: @user, title: title)
    assert_equal second_category.slug, "#{title.parameterize}-2"
    first_category.destroy
    second_category.update!(title: "general-2")

    expected_slug_suffix_for_new_category = second_category.slug.split("-").last.to_i + 1
    new_category = create(:category, user: @user, title: title)
    assert_equal new_category.slug, "#{title.parameterize}-#{expected_slug_suffix_for_new_category}"
end

  def test_existing_slug_prefixed_in_new_category_title_doesnt_break_slug_generation
    title_having_new_title_as_substring = "buy milk and apple"
    new_title = "buy milk"

    existing_category = @organization.user.categories.create!(title: title_having_new_title_as_substring, user: @user)
    assert_equal title_having_new_title_as_substring.parameterize, existing_category.slug

    new_category = @organization.user.categories.create!(title: new_title, user: @user)
    assert_equal new_title.parameterize, new_category.slug
  end

  def test_having_numbered_slug_substring_in_title_doesnt_affect_slug_generation
    title_with_numbered_substring = "buy 2 apples"

    existing_category = @organization.user.categories.create!(title: title_with_numbered_substring, user: @user)
    assert_equal title_with_numbered_substring.parameterize, existing_category.slug

    substring_of_existing_slug = "buy"
    new_category = @organization.user.categories.create!(title: substring_of_existing_slug, user: @user)

    assert_equal substring_of_existing_slug.parameterize, new_category.slug
  end

  def test_category_should_not_be_valid_without_user
    @category.user = nil
    assert_not @category.save
    assert_includes t("must_exists", entity: "User"), @category.errors.full_messages.to_sentence
  end
end
