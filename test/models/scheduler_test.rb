# frozen_string_literal: true

require "test_helper"

class SchedulerTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
    @scheduler = create(:scheduler, article: @article)
  end

  def test_schedule_should_not_valid_without_status
    @scheduler.status = ""
    assert_not @scheduler.valid?
    assert_includes @scheduler.errors.full_messages.to_sentence, t("article.schedule.not_blank", field: "Status")
  end

  def test_schedule_should_not_valid_with_invalid_status
    invalid_status = "invalid"
    new_article = create(:article, categories: [@category], user: @user)
    new_schedule = build(:scheduler, article: new_article, status: invalid_status)
    assert_not new_schedule.valid?
    assert_equal t("article.schedule.invalid_status"),
      new_schedule.errors.full_messages.to_sentence
  end

  def test_scheduling_date_time_should_not_fall_behind_current_date_time
    new_article = create(:article, categories: [@category], user: @user)
    new_schedule = build(:scheduler, article: new_article, scheduling_date_time: "12/01/2022 10")
    assert_not new_schedule.valid?
    assert_equal t("not_possible", entity: "Scheduling"), new_schedule.errors.full_messages.to_sentence
  end

  def test_article_can_have_only_unique_pair_of_schedules
    new_article = create(:article, categories: [@category], user: @user)
    new_schedule_one = create(
      :scheduler, article: new_article, scheduling_date_time: @scheduler.scheduling_date_time,
      status: "published")
    new_schedule_two = create(
      :scheduler, article: new_article, scheduling_date_time: new_schedule_one.scheduling_date_time + 1.day,
      status: "draft")
    new_schedule_three = build(
      :scheduler, article: new_article, scheduling_date_time: new_schedule_two.scheduling_date_time + 1.day,
      status: "draft")
    assert_not new_schedule_three.valid?
    assert_includes new_schedule_three.errors.full_messages.to_sentence,
      t("article.schedule.already_exists", entity: "Article")
  end

  def test_scheduling_not_possible_if_scheduling_status_and_current_article_status_is_same
    new_article = create(:article, categories: [@category], user: @user)
    new_schedule = build(:scheduler, article: new_article, status: :draft)
    assert_not new_schedule.valid?
    assert_equal t("not_possible", entity: "Scheduling"), new_schedule.errors.full_messages.to_sentence
  end

  def test_scheduling_should_not_be_valid_without_article
    assert_raises NoMethodError do
      @scheduler.update!(article: nil)
    end

    error_msg = @scheduler.errors.full_messages.to_sentence
    assert_equal t("must_exists", entity: "Article"), error_msg
  end

  def test_second_schedule_date_time_should_not_fall_behind_first_schedule
    new_article = create(:article, categories: [@category], user: @user)
    new_schedule_one = create(
      :scheduler, article: new_article, scheduling_date_time: @scheduler.scheduling_date_time,
      status: "published")
    new_schedule_two = build(
      :scheduler, article: new_article, scheduling_date_time: new_schedule_one.scheduling_date_time - 1.day,
      status: "draft")
    assert_not new_schedule_two.valid?
    assert_equal t("article.schedule.invalid_time"), new_schedule_two.errors.full_messages.to_sentence
  end

  def test_schedule_must_raise_error_if_two_schedules_having_same_status
    new_article = create(:article, categories: [@category], user: @user)
    new_schedule_one = create(
      :scheduler, article: new_article, scheduling_date_time: @scheduler.scheduling_date_time,
      status: "published")
    new_schedule_two = build(
      :scheduler, article: new_article, scheduling_date_time: new_schedule_one.scheduling_date_time + 1.day,
      status: "published")

    assert_not new_schedule_two.valid?
    assert_includes new_schedule_two.errors.full_messages.to_sentence,
      t("article.schedule.already_exists", entity: "Article")
  end
end
