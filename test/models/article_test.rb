# frozen_string_literal: true

require "test_helper"

class ArticleTest < ActiveSupport::TestCase
  def setup
    @draft_article = create(:article, :article_with_categories)
    @published_article = create(:article, :article_with_categories, status: :published)
  end

  def test_article_should_not_be_valid_without_categories
    @draft_article.categories = []
    assert_not @draft_article.valid?
    assert_includes @draft_article.errors.full_messages, "Categories #{t("article.categories.is_not_empty")}"
  end

  def test_article_should_not_be_valid_without_title
    @draft_article.title = ""
    assert_not @draft_article.valid?
    assert_includes @draft_article.errors.full_messages.to_sentence, t("article.not_blank", field: "Title")
  end

  def test_article_title_should_start_with_alphanumeric_character
    @draft_article.title = "/"
    assert_not @draft_article.valid?
    assert_equal t("article.invalid", entity: "Title"), @draft_article.errors.full_messages.to_sentence
  end

  def test_article_should_not_be_valid_without_body
    @draft_article.body = ""
    assert_not @draft_article.valid?
    assert_equal t("article.not_blank", field: "Body"), @draft_article.errors.full_messages.to_sentence
  end

  def test_article_title_should_not_exceed_maximum_length
    @draft_article.title = "a" * (Article::MAX_TITLE_LENGTH + 1)
    assert_not @draft_article.valid?
  end

  def test_article_slug_is_parameterized_title
    title = @published_article.title
    assert_equal title.parameterize, @published_article.slug
  end

  def test_incremental_slug_generation_for_articles_with_duplicate_two_worded_titles
    first_article = create(
      :article, :article_with_categories, title: "test article", status: :published)
    second_article = create(
      :article, :article_with_categories, title: "test article", status: :published)

    assert_equal "test-article", first_article.slug
    assert_equal "test-article-2", second_article.slug
  end

  def test_incremental_slug_generation_for_articles_with_duplicate_hyphenated_titles
    first_article = create(:article, :article_with_categories, title: "test-article", status: :published)
    second_article = create(:article, :article_with_categories, title: "test-article", status: :published)

    assert_equal "test-article", first_article.slug
    assert_equal "test-article-2", second_article.slug
  end

  def test_slug_generation_for_articles_having_titles_one_being_prefix_of_the_other
    first_article = create(:article, :article_with_categories, title: "fishing", status: :published)
    second_article = create(:article, :article_with_categories, title: "fish", status: :published)

    assert_equal "fishing", first_article.slug
    assert_equal "fish", second_article.slug
  end

  def test_error_raised_for_duplicate_slug
    categories = [create(:category)]
    another_test_article = create(
      :article, :article_with_categories, title: "another test article", status: :published)
    assert_raises ActiveRecord::RecordInvalid do
      another_test_article.update!(slug: @published_article.slug)
    end

    error_msg = another_test_article.errors.full_messages.to_sentence
    assert_match t("article.slug.immutable"), error_msg
  end

  def test_updating_title_does_not_update_slug
    assert_no_changes -> { @published_article.reload.slug } do
      updated_article_title = "updated article title"
      @published_article.update!(title: updated_article_title)
      assert_equal updated_article_title, @published_article.title
    end
  end

  def test_last_published_at_should_present_once_the_article_is_published
    assert_not_nil @published_article.last_published_at
  end

  def test_last_published_at_should_not_present_when_article_is_draft_initially
    assert_nil @draft_article.last_published_at
  end

  def test_slug_suffix_is_maximum_slug_count_plus_one_if_two_or_more_slugs_already_exist
    title = "test-article"
    first_article = create(:article, :article_with_categories, title: title, status: :published)
    second_article = create(:article, :article_with_categories, title: title, status: :published)
    third_article = create(:article, :article_with_categories, title: title, status: :published)
    fourth_article = create(:article, :article_with_categories, title: title, status: :published)

    assert_equal fourth_article.slug, "#{title.parameterize}-4"
    third_article.destroy
    expected_slug_suffix_for_new_article = fourth_article.slug.split("-").last.to_i + 1
    new_article = create(:article, :article_with_categories, title: title, status: :published)
    assert_equal new_article.slug, "#{title.parameterize}-#{expected_slug_suffix_for_new_article}"
  end

  def test_existing_slug_prefixed_in_new_article_title_doesnt_break_slug_generation
    title_having_new_title_as_substring = "buy milk and apple"
    new_title = "buy milk"

    existing_article = create(
      :article, :article_with_categories, title: title_having_new_title_as_substring, status: :published)
    assert_equal title_having_new_title_as_substring.parameterize, existing_article.slug

    new_article = create(:article, :article_with_categories, title: new_title, status: :published)
    assert_equal new_title.parameterize, new_article.slug
  end

  def test_having_numbered_slug_substring_in_title_doesnt_affect_slug_generation
    title_with_numbered_substring = "buy 2 apples"

    existing_article = create(
      :article, :article_with_categories,
      title: title_with_numbered_substring, status: :published)
    assert_equal title_with_numbered_substring.parameterize, existing_article.slug

    substring_of_existing_slug = "buy"
    new_article = create(:article, :article_with_categories, title: substring_of_existing_slug, status: :published)

    assert_equal substring_of_existing_slug.parameterize, new_article.slug
  end

  def test_creates_multiple_articles_with_unique_slug
    articles = create_list(:article, 10, :article_with_categories, status: :published)
    slugs = articles.pluck(:slug)
    assert_equal slugs.uniq, slugs
  end

  def test_article_can_be_created_with_multiple_categories
    new_category_one = create(:category)
    new_category_two = create(:category)
    new_category_three = create(:category)

    assert_difference ["ArticleCategory.count"], 3 do
      create(:article, categories: [new_category_one, new_category_two, new_category_three])
    end
  end

  def test_should_not_generate_slug_for_draft_article
    slug = @draft_article.slug
    assert_equal slug, nil
  end

  def test_article_should_not_be_valid_without_user
    @published_article.user = nil
    assert_not @published_article.save
    assert_includes t("must_exists", entity: "User"), @published_article.errors.full_messages.to_sentence
  end

  def test_article_should_not_be_valid_without_categories
    @published_article.categories = []
    assert_not @published_article.save

    assert_includes @published_article.errors.full_messages.to_sentence,
      t("article_categories.not_blank", entity: "Article categories")
  end
end
