# frozen_string_literal: true

require "test_helper"

class VisitTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
    @visit = create(:visit, article: @article)
  end

  def test_visit_should_not_be_valid_without_article
    assert_raises ActiveRecord::RecordInvalid do
      @visit.update!(article: nil)
    end

    error_msg = @visit.errors.full_messages.to_sentence
    assert_equal t("must_exists", entity: "Article"), error_msg
  end
end
