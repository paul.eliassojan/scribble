# frozen_string_literal: true

require "test_helper"

class RedirectionTest < ActiveSupport::TestCase
  def setup
    @redirection = create(:redirection)
  end

  def test_redirection_should_not_be_valid_without_to_path
    @redirection.to_path = ""
    assert_not @redirection.valid?
    assert_includes @redirection.errors.full_messages.to_sentence, t("redirection.not_blank", entity: "To path"),
      t("redirection.invalid", field: "To path")
  end

  def test_redirection_should_not_be_valid_without_from_path
    @redirection.from_path = ""
    assert_not @redirection.valid?
    assert_includes @redirection.errors.full_messages.to_sentence, t("redirection.not_blank", entity: "From path"),
      t("redirection.invalid", field: "From path")
  end

  def test_redirection_should_start_with_a_alphanumeric_character
    @redirection.from_path = "()"
    assert_not @redirection.valid?
    assert_includes t("redirection.invalid", field: "From path"), @redirection.errors.full_messages.to_sentence
  end

  def test_redirection_from_path_must_be_unique
    redirection = build(:redirection, from_path: @redirection.from_path)
    assert_not redirection.valid?
    assert_includes t("redirection.already_taken", field: "From path"), redirection.errors.full_messages.to_sentence
  end

  def test_redirection_shouldnt_be_valid_without_alphanumeric_character_in_starting
    redirection = build(:redirection, from_path: "/about", to_path: "contact")
    assert_not redirection.valid?
    assert_equal t("redirection.invalid", field: "From path"), redirection.errors.full_messages.to_sentence
  end

  def test_redirection_from_path_and_to_path_shouldnt_be_same
    redirection = build(:redirection)

    redirection = build(:redirection, to_path: redirection.to_path, from_path: redirection.to_path)
    assert_not redirection.valid?
    assert_includes t("redirection.cannot_create", entity: "Redirection"), redirection.errors.full_messages.to_sentence
  end

  def test_should_not_create_redirection_incase_of_simple_redirection_loop
    new_redirection = build(:redirection, from_path: @redirection.to_path, to_path: @redirection.from_path)
    assert_not new_redirection.valid?
    assert_equal t("redirection.cannot_create", entity: "Redirection"), new_redirection.errors.full_messages.to_sentence
  end

  def test_should_not_create_redirection_incase_of_cyclic_loop
    new_redirection_one = create(:redirection, from_path: @redirection.to_path, to_path: "rails")
    new_redirection_two = build(:redirection, from_path: new_redirection_one.to_path, to_path: @redirection.from_path)
    assert_not new_redirection_two.valid?
    assert_equal t("redirection.cannot_create", entity: "Redirection"),
      new_redirection_two.errors.full_messages.to_sentence
  end

  def test_should_create_redirection_if_from_path_and_to_path_already_exists_and_should_not_create_cycle
    new_redirection_one = build(:redirection, from_path: "1", to_path: "2")
    new_redirection_two = build(:redirection, from_path: "3", to_path: "4")
    new_redirection_three = build(:redirection, from_path: "4", to_path: "1")
    new_redirection_four = build(:redirection, from_path: "2", to_path: "5")

    assert_equal new_redirection_three.valid?, true
  end

  def test_redirection_should_not_be_valid_without_user
    @redirection.user = nil
    assert_not @redirection.save
    assert_includes t("must_exists", entity: "User"), @redirection.errors.full_messages.to_sentence
  end
end
