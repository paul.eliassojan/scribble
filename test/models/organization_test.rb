# frozen_string_literal: true

require "test_helper"

class OrganizationTest < ActiveSupport::TestCase
  def setup
    @organization = create(:organization)
  end

  def test_organization_should_not_be_valid_without_name
    @organization.name = ""
    assert_not @organization.valid?
    assert_includes t("organization.not_blank", field: "Name"), @organization.errors.full_messages.to_sentence
  end

  def test_organization_name_should_not_exceed_maximum_length
    @organization.name = "a" * (Organization::MAX_NAME_LENGTH + 1)
    assert_not @organization.valid?
    assert_includes t("organization.too_long", field: "Name", length: Organization::MAX_NAME_LENGTH),
      @organization.errors.full_messages.to_sentence
  end

  def test_organization_password_should_satisfy_minimum_length
    @organization.password = "a" * (Organization::MIN_PASSWORD_LENGTH - 1)
    assert_not @organization.valid?
    assert_includes @organization.errors.full_messages.to_sentence,
      t("organization.too_short", field: "Password", length: Organization::MIN_PASSWORD_LENGTH),
      t("organization.invalid", field: "Password")
  end

  def test_organization_should_have_unique_auth_token
    @organization.save!
    second_organization = create(:organization)
    assert_not_same @organization.authentication_token, second_organization.authentication_token
  end

  def test_default_password_protection_can_only_be_boolean
    @organization.password_protected = ""
    assert_not @organization.valid?
    assert_equal t("organization.password_protected.is_boolean"), @organization.errors.full_messages.to_sentence
  end

  def test_password_should_contain_atleast_one_number_and_one_alphabet
    @organization.password = "macbook"
    assert_not @organization.valid?
    assert_equal t("organization.invalid", field: "Password"), @organization.errors.full_messages.to_sentence
  end
end
