# frozen_string_literal: true

require "test_helper"

class RedirectionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @redirection = create(:redirection, user: @user)
  end

  def test_should_create_redirection
    redirection = build(:redirection)

    post api_redirections_path,
      params: { redirection: { to_path: redirection.to_path, from_path: redirection.from_path } },
      headers: headers
    assert_response :success
    assert_equal t("successfully_created", entity: "Redirection"), response_body["notice"]
  end

  def test_should_list_all_redirections
    get api_redirections_path, headers: headers
    assert_response :success

    redirections_response = response_body["redirections"]
    redirections = @organization.user.redirections
    assert_equal redirections.pluck(:id), redirections_response.pluck("id")
  end

  def test_should_update_any_redirection_field
    new_to_path = "#{@redirection.to_path}-updated"
    put api_redirection_path(@redirection.id), params: { redirection: { to_path: new_to_path } }, headers: headers

    assert_response :success
    @redirection.reload
    assert_equal @redirection.to_path, new_to_path
    assert_equal t("successfully_updated", entity: "Redirection"), response_body["notice"]
  end

  def test_should_destroy_redirection
    assert_difference "@organization.user.redirections.count", -1 do
      delete api_redirection_path(@redirection.id), headers: headers
    end
    assert_response :ok
    assert_equal t("successfully_deleted", entity: "Redirection"), response_body["notice"]
  end
end
