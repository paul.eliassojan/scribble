# frozen_string_literal: true

require "test_helper"

class Api::ArticleAnalyticsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user, status: :published)
  end

  def test_article_analytics_should_list_all_published_articles_per_page_and_limit
    page_limit = 5
    page = 1
    create(:article, categories: [@category], user: @user, status: :published)
    get api_analytics_path, params: { page: page, limit: page_limit }, headers: headers
    assert_response :success

    article_analytics_response = response_body["articles"]
    published_articles = @user.articles.published.page(page).per(page_limit)
    assert_equal published_articles.pluck(:id).sort, article_analytics_response.pluck("id").sort
  end

  def test_article_analytics_should_show_article_visits_per_day
    2.times do
      get api_public_article_path(@article.slug), params: { category: @category.slug }, headers: headers
      assert_response :success
    end
    get api_analytic_path(@article.id), headers: headers
    assert_response :success

    article_visits_response = response_body["article_visits"]
    article_visits = @article.visits.group("DATE(created_at)").select(
      "DATE(created_at),COUNT(created_at) as visits").as_json

    article_visits.size.times do |index|
      assert_equal article_visits[index]["date"], article_visits_response[index]["date"]
      assert_equal article_visits[index]["visits"], article_visits_response[index]["visits"]
    end
  end
end
