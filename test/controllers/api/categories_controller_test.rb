# frozen_string_literal: true

require "test_helper"

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
  end

  def test_should_create_a_category
    post api_categories_path,
      params: { category: { title: "Getting Started" } },
      headers: headers

    assert_response :success
    assert_equal t("successfully_created", entity: "Category"), response_body["notice"]
  end

  def test_should_list_all_categories
    create(:category, user: @user)
    get api_categories_path, headers: headers
    assert_response :success

    categories_response = response_body["categories"]
    categories = @organization.user.categories
    assert_equal categories.pluck(:id).sort, categories_response.pluck("id").sort
  end

  def test_update_category_title_field
    new_title = "#{@category.title}-updated"
    put api_category_path(@category.id), params: { category: { title: new_title } }, headers: headers

    assert_response :success
    @category.reload
    assert_equal @category.title, new_title
    assert_equal t("successfully_updated", entity: "Category"), response_body["notice"]
  end

  def test_should_destroy_category
    new_category = create(:category, title: "test", user: @user)
    assert_difference "@organization.user.categories.count", -1 do
      delete api_category_path(@category.id), params: { category_id: new_category.id }, headers: headers
    end
    assert_response :success
    assert_equal t("successfully_deleted", entity: "Category"), response_body["notice"]
  end

  def test_category_should_not_destroy_on_invalid_url_params
    category = create(:category, title: "test", user: @user)
    invalid_category_id = -1

    delete api_category_path(category.id), params: { category_id: invalid_category_id }, headers: headers
    assert_response :not_found
    assert_equal t("not_found", entity: "Category"), response_body["error"]
  end

  def test_should_update_category_position_when_postion_change_is_requested
    new_category = create(:category, title: "rails", user: @user)
    new_category_position = new_category.position
    put api_category_path(new_category.id), params: { category: { position: 1 } }, headers: headers

    new_category.reload
    assert_equal new_category.position, 1
    assert_equal t("successfully_updated", entity: "Category"), response_body["notice"]
  end
end
