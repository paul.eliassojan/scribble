# frozen_string_literal: true

require "test_helper"

class Api::ArticleVersionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
    @scheduler = create(:scheduler, article: @article)
  end

  def test_should_list_all_article_versions
    @article.update!(title: "version-history")
    @article.update!(body: "test-body")

    get api_article_versions_path(@article.id), headers: headers
    assert_response :success
    article_versions_response = response_body["article_versions"]
    assert_equal @article.versions.pluck(:id).drop(1).sort, article_versions_response.pluck("id").sort
  end

  def test_should_show_particular_article_version
    @article.update!(title: "version-history")
    article_version = @article.versions.second

    get api_article_version_path(@article.id, article_version.id), headers: headers
    assert_response :success
    article_version_response = response_body
    assert_equal article_version.id, article_version_response["id"]
  end

  def test_should_restore_article_version_as_current_article
    @article.update!(title: "react-version")
    @article.update!(title: "rails-version")

    article_version = @article.versions.third
    put api_article_version_path(@article.id, article_version.id), headers: headers
    assert_response :success
    @article.reload
    assert_equal article_version.reify.title, @article.title
  end

  def test_should_change_article_status_to_draft_when_restoring_an_article_version
    @article.update!(title: "react-version", status: :published)
    @article.update!(title: "rails-version")

    article_version = @article.versions.third
    put api_article_version_path(@article.id, article_version.id), headers: headers
    @article.reload

    assert_equal "draft", @article.status
  end

  def test_should_update_restore_time_of_article_when_restoring_an_article_version
    @article.update!(title: "react-version")

    article_version = @article.versions.second
    put api_article_version_path(@article.id, article_version.id), headers: headers
    assert_response :success
    @article.reload
    assert_equal article_version.created_at.strftime("%Y-%m-%d %H:%M:%S UTC"), @article.restore_time
  end

  def test_should_destroy_article_schedule_when_status_of_restoring_version_matches_scheduler_status
    new_article = create(:article, categories: [@category], user: @user, status: :published)
    new_schedule_one = create(
      :scheduler, article: new_article, scheduling_date_time: @scheduler.scheduling_date_time + 1.day,
      status: "draft")
    new_schedule_two = create(
      :scheduler, article: new_article, scheduling_date_time: new_schedule_one.scheduling_date_time + 1.day,
      status: "published")

    schedule_to_delete_id = new_article.schedulers.where(
      "scheduling_date_time > ?",
      Time.zone.now).where(status: "draft").pluck(:id)

    new_article.update!(title: "react-version")
    new_article_version = new_article.versions.second
    assert_difference "new_article.schedulers.count", -1 do
      put api_article_version_path(new_article.id, new_article_version.id), headers: headers
    end
    assert_response :success
    assert_not_includes schedule_to_delete_id, new_article.schedulers.pluck(:id)
  end
end
