# frozen_string_literal: true

require "test_helper"

class SessionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
  end

  def test_should_login_organization_with_valid_credentials
    post api_session_path, params: { session: { name: @organization.name, password: @organization.password } }
    assert_response :success
    assert_equal session[:auth], @organization.authentication_token
  end

  def test_shouldnt_login_organization_with_invalid_credentials
    post api_session_path, params: { session: { name: @organization.name, password: "invalid password" } }
    assert_response :unauthorized
    assert_equal t("session.incorrect_credentials"), response_body["error"]
  end
end
