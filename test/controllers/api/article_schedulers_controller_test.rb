# frozen_string_literal: true

require "test_helper"

class Api::ArticleSchedulersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
    @scheduler = create(:scheduler, article: @article)
  end

  def test_should_receive_article_schedule
    get api_article_schedulers_path(@article.id), headers: headers
    assert_response :success

    schedule = @article.schedulers.where("scheduling_date_time > ?", Time.zone.now)
    schedule_response = response_body["article_schedules"]
    assert_equal schedule.pluck(:id), schedule_response.pluck("id")
  end

  def test_should_create_article_schedule
    new_article = create(:article, categories: [@category], user: @user, status: :draft)

    post api_article_schedulers_path(new_article.id),
      params: {
        scheduler: {
          scheduling_date_time: @scheduler.scheduling_date_time,
          status: :published
        }
      }, headers: headers
    assert_response :success
    assert_equal t("successfully_scheduled", entity: "Article"), response_body["notice"]
  end

  def test_should_not_create_article_schedule_for_invalid_article_id
    invalid_article_id = "1234"
    post api_article_schedulers_path(invalid_article_id),
      params: {
        scheduler: {
          scheduling_date_time: @scheduler.scheduling_date_time,
          status: :published
        }
      }, headers: headers
    assert_response :not_found
    assert_equal t("not_found", entity: "Article"), response_body["error"]
  end

  def test_should_destroy_article_schedule_if_deleting_schedule_status_matches_current_article_status
    assert_difference "@article.schedulers.count", -1 do
      delete api_article_scheduler_path(@article.id, @scheduler.id), headers: headers
    end
    assert_response :ok
    assert_equal t("successfully_deleted", entity: "Schedule"), response_body["notice"]
  end

  def test_should_destroy_all_article_schedules_left_behind_when_schedule_matches_current_article_status
    new_article = create(:article, categories: [@category], user: @user)
    new_schedule_one = create(
      :scheduler, article: new_article, scheduling_date_time: @scheduler.scheduling_date_time,
      status: "published")
    new_schedule_two = create(
      :scheduler, article: new_article, scheduling_date_time: new_schedule_one.scheduling_date_time + 1.day,
      status: "draft")
    assert_difference "new_article.schedulers.count", -2 do
      delete api_article_scheduler_path(new_article.id, new_schedule_one.id), headers: headers
    end
    assert_response :ok
  end
end
