# frozen_string_literal: true

require "test_helper"

class OrganizationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
  end

  def test_should_update_organization_name
    new_name = "test-name"
    put api_organization_path, params: { organization: { name: new_name } }, headers: headers
    assert_response :success
    @organization.reload

    assert_equal @organization.name, new_name
  end

  def test_should_update_organization_password
    new_password = "macbook12"
    put api_organization_path, params: { organization: { password: new_password } }, headers: headers

    assert_response :success
    assert_equal t("successfully_updated", entity: "Organization"), response_body["notice"]
  end

  def test_should_able_to_update_password_protected
    put api_organization_path, params: { organization: { password_protected: true } },
      headers: headers

    assert_response :success
    @organization.reload
    assert_equal @organization.password_protected, true
    assert_equal t("successfully_updated", entity: "Organization"), response_body["notice"]
  end

  def test_should_able_to_show_current_organization
    get api_organization_path, headers: headers
    assert_response :success
    assert_includes @organization.name, response_body["name"]
  end

  def test_should_clear_previous_session_if_organization_is_updated
    post api_session_path, params: { session: { name: @organization.name, password: @organization.password } },
      headers: headers
    assert_response :success

    put api_organization_path, params: { organization: { password_protected: false } },
      headers: headers
    assert_response :success
    assert_nil session[:auth]
  end
end
