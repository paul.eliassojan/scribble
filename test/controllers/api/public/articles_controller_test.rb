# frozen_string_literal: true

require "test_helper"

class Public::ArticlesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user, status: :published)
  end

  def login(name, password)
    post api_session_path, params: { session: { name: name, password: password } }
  end

  def test_should_show_article_under_particular_category_in_public_if_session_is_valid
    @organization.update(password_protected: true)
    login(@organization.name, @organization.password)

    get api_public_article_path(@article.slug), params: { category: @category.slug }, headers: headers
    assert_response :success
    category_title = @organization.user.categories.first.title
    assert_equal @article.slug, response_body["article"]["slug"]
    assert_equal category_title, response_body["article"]["category"]
  end

  def test_should_not_show_draft_article_under_category_in_public_if_session_is_valid
    article = create(:article, categories: [@category], user: @user, status: :draft)
    @organization.update(password_protected: true)
    login(@organization.name, @organization.password)

    get api_public_article_path(article.id), params: { category: @category.slug }, headers: headers
    assert_response :not_found
    assert_equal t("not_found", entity: "Article"), response_body["error"]
  end

  def test_should_able_to_show_published_article_if_password_protection_is_false_in_public
    get api_public_article_path(@article.slug), params: { category: @category.slug }, headers: headers
    assert_response :success
    assert_equal @article.slug, response_body["article"]["slug"]
  end

  def test_shouldnt_show_published_article_if_session_is_invalid
    @organization.update(password_protected: true)
    get api_public_article_path(@article.slug), params: { category: @category.slug }, headers: headers

    assert_response :unprocessable_entity
    assert_equal t("session.could_not_auth"), response_body["error"]
  end

  def test_should_raise_error_if_published_article_is_not_under_specified_category_for_valid_session
    new_category_slug = "test-slug"
    @organization.update(password_protected: true)
    login(@organization.name, @organization.password)

    get api_public_article_path(@article.slug), params: { category: new_category_slug }, headers: headers
    assert_response :not_found
    assert_includes t("not_found", entity: "Category"), response_body["error"]
  end

  def test_should_filter_public_articles_based_on_search_query
    new_article = create(:article, categories: [@category], user: @user, status: :published)

    get api_public_articles_path, params: { search: new_article.title }, headers: headers
    assert_response :success
    public_articles_response = response_body["articles"].flatten
    filtered_articles = @organization.user.articles.published.where(
      "lower(title) LIKE ?",
      "%#{new_article.title.downcase}%")
    assert_equal filtered_articles.pluck(:slug), public_articles_response.pluck("article_slug")
  end

  def test_should_increase_visits_when_visiting_article
    assert_difference "@article.visits.count", 1 do
      get api_public_article_path(@article.slug), params: { category: @category.slug }, headers: headers
    end
    assert_response :success
  end

  def test_should_show_initial_articles_while_initiating_search
    new_category = create(:category, user: @user)

    create(:article, categories: [@category], user: @user, status: :published)
    create(:article, categories: [@category], user: @user, status: :published)
    create(:article, categories: [@category], user: @user, status: :published)
    create(:article, categories: [new_category], user: @user, status: :published)
    create(:article, categories: [new_category], user: @user, status: :published)

    get api_public_articles_path, headers: headers
    assert_response :success

    initial_articles = @organization.user.articles.published.where(
      "lower(title) LIKE ?",
      "%").limit(Article::PUBLIC_SEARCH_LIMIT)
    initial_articles_slug = initial_articles.pluck(:slug)
    articles_response_slug = response_body["articles"].flatten.pluck("article_slug")
    assert_equal initial_articles_slug.sort, articles_response_slug.sort
  end

  def test_should_not_show_public_articles_for_unsuccessful_login
    @organization.update(password_protected: true)
    invalid_password = "1234"
    login(@organization.name, invalid_password)
    assert_response :unauthorized

    get api_public_article_path(@article.slug), params: { category: @category.slug }, headers: headers
    assert_response :unprocessable_entity
    assert_equal t("session.could_not_auth"), response_body["error"]
  end
end
