# frozen_string_literal: true

require "test_helper"

class Public::CategoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
  end

  def login(name, password)
    post api_session_path, params: { session: { name: name, password: password } }
  end

  def test_should_list_all_categories_having_published_articles_if_session_is_valid
    new_category = create(:category, user: @user)
    create(:article, categories: [new_category], user: @user, status: :published)

    create(:article, categories: [@category], user: @user, status: :published)
    login(@organization.name, @organization.password)

    @organization.update(password_protected: true)

    get api_public_categories_path, headers: headers
    assert_response :success
    categories_response = response_body["categories"]
    categories = @organization.user.categories.select { |category| category.articles.published.length > 0 }
    assert_equal categories.pluck(:slug).sort, categories_response.pluck("slug").sort
  end

  def test_should_list_all_published_articles_under_particular_category_if_session_is_valid
    @organization.update(password_protected: true)
    create(:article, categories: [@category], user: @user, status: :published)
    create(:article, categories: [@category], user: @user, status: :published)
    login(@organization.name, @organization.password)

    get api_public_category_path(@category.slug), headers: headers
    assert_response :success
    articles_response = response_body["category_articles"]
    published_articles = @organization.user.articles.select { |article| article.published? }
    assert_equal published_articles.pluck(:slug).sort, articles_response.pluck("slug").sort
  end

  def test_shouldnt_list_published_articles_under_invalid_category
    @organization.update(password_protected: true)
    invalid_category_id = 233
    login(@organization.name, @organization.password)

    get api_public_category_path(invalid_category_id), headers: headers
    assert_response :not_found
    assert_equal t("not_found", entity: "Category"), response_body["error"]
  end

  def test_shouldnt_list_categories_without_published_articles_if_session_is_valid
    @organization.update(password_protected: true)
    create(:article, categories: [@category], user: @user, status: :draft)
    login(@organization.name, @organization.password)

    get api_public_categories_path, headers: headers
    assert_response :success
    assert_not_same response_body["categories"].size, @organization.user.categories.count
  end

  def test_should_not_list_categories_if_session_is_invalid
    @organization.update(password_protected: true)
    get api_public_categories_path, headers: headers
    assert_response :unprocessable_entity
    assert_equal t("session.could_not_auth"), response_body["error"]
  end

  def test_should_list_all_categories_having_published_articles_if_password_protection_is_false
    create(:article, categories: [@category], user: @user, status: :published)
    create(:article, categories: [@category], user: @user, status: :published)

    get api_public_categories_path, headers: headers
    assert_response :success
    categories_response = response_body["categories"]
    categories = @organization.user.categories
    assert_equal categories_response.pluck("slug"), categories.pluck(:slug)
  end

  def test_should_not_list_published_articles_under_particular_category_if_session_is_invalid
    @organization.update(password_protected: true)
    create(:article, categories: [@category], user: @user, status: :published)

    get api_public_category_path(@category.slug), headers: headers
    assert_response :unprocessable_entity
    assert_equal t("session.could_not_auth"), response_body["error"]
  end

  def test_not_found_error_rendered_for_invalid_category_slug
    invalid_slug = "invalid-slug"

    get api_public_category_path(invalid_slug), headers: headers
    assert_response :not_found
    assert_equal t("not_found", entity: "Category"), response_body["error"]
  end
end
