# frozen_string_literal: true

require "test_helper"

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    @category = create(:category, user: @user)
    @article = create(:article, categories: [@category], user: @user)
    @scheduler = create(:scheduler, article: @article)
  end

  def test_should_create_an_article
    post api_articles_path,
      params: { article: { title: "Welcome", body: "hello", category_ids: [@category.id] } },
      headers: headers
    assert_response :success
    assert_equal t("successfully_created", entity: "Article"), response_body["notice"]
  end

  def test_should_create_an_article_with_publish_later_schedule
    post api_articles_path,
      params: {
        article: { title: @article.title, body: @article.body, category_ids: [@category.id] }, scheduler: {
          scheduling_date_time: @scheduler.scheduling_date_time,
          status: :published
        }, quiet: true
      },
      headers: headers
    assert_response :success
    assert_equal t("successfully_created", entity: "Article"), response_body["notice"]
  end

  def test_should_not_create_unpublish_later_schedule_while_creating_an_article
    post api_articles_path,
      params: {
        article: { title: @article.title, body: @article.body, category_ids: [@category.id] }, scheduler: {
          scheduling_date_time: @scheduler.scheduling_date_time,
          status: :draft
        }, quiet: true
      },
      headers: headers
    assert_response :unprocessable_entity
    assert_equal t("not_possible", entity: "Scheduling"), response_body["error"]
  end

  def test_should_not_create_schedule_with_past_date_while_creating_an_article
    post api_articles_path,
      params: {
        article: { title: @article.title, body: @article.body, category_ids: [@category.id] }, scheduler: {
          scheduling_date_time: "12/01/2020 10",
          status: :published
        }, quiet: true
      },
      headers: headers
    assert_response :unprocessable_entity
    assert_equal t("not_possible", entity: "Scheduling"), response_body["error"]
  end

  def test_should_list_all_articles
    get api_articles_path, headers: headers
    assert_response :success

    articles_response = response_body["articles"]
    articles = @organization.user.articles
    assert_equal articles.pluck(:id).sort, articles_response.pluck("id").sort
  end

  def test_should_retrive_an_article
    get api_article_path(@article.id), headers: headers
    assert_response :success
    assert_equal @article.id, response_body["id"]
  end

  def test_should_destroy_article_schedule_when_status_of_updating_article_matches_scheduled_state
    new_schedule = create(
      :scheduler, article: @article, scheduling_date_time: @scheduler.scheduling_date_time + 1.day,
      status: "draft")

    new_title = "#{@article.title}-(updated)"
    new_body = "#{@article.body}-(updated)"
    new_status = @scheduler.status

    schedule_to_delete_id = @article.schedulers.where(
      "scheduling_date_time > ?",
      Time.zone.now).where(status: @scheduler.status).pluck(:id)

    article_params = { article: { title: new_title, body: new_body, status: new_status } }
    assert_difference "@article.schedulers.count", -1 do
      put api_article_path(@article.id), params: article_params, headers: headers
    end
    assert_response :success
    assert_not_includes schedule_to_delete_id, @article.schedulers.pluck(:id)
  end

  def test_should_destroy_article
    assert_difference "@organization.user.articles.count", -1 do
      delete api_article_path(@article.id), headers: headers
    end
    assert_response :ok
    assert_equal t("successfully_deleted", entity: "Article"), response_body["notice"]
  end

  def test_shouldnt_create_article_with_invalid_status
    article_params = { article: { title: "Welcome", body: "test", category_ids: [@category.id], status: "demo" } }

    post api_articles_path, params: article_params, headers: headers
    assert_response :unprocessable_entity
    assert_equal t("article.status.invalid", entity: "demo"), response_body["error"]
  end

  def test_shouldnt_create_article_with_invalid_category
    post api_articles_path, params: { article: { title: "Welcome", body: "test", category_ids: [-2] } },
      headers: headers

    assert_response :not_found
    assert_equal t("not_found", entity: "Category"), response_body["error"]
  end

  def test_update_any_article_fields
    new_title = "#{@article.title}-(updated)"
    new_body = "#{@article.body}-(updated)"
    new_status = "published"

    article_params = { article: { title: new_title, body: new_body, status: :published } }
    put api_article_path(@article.id), params: article_params, headers: headers

    assert_response :success
    @article.reload
    assert_equal @article.title, new_title
    assert_equal @article.body, new_body
    assert_equal @article.status, new_status
    assert_equal t("successfully_updated", entity: "Article"), response_body["notice"]
  end
end
