# frozen_string_literal: true

require "test_helper"

class HomeControllerTest < ActionDispatch::IntegrationTest
  def setup
    @redirection = create(:redirection)
  end

  def test_should_get_successfully_from_root_url
    get root_path
    assert_response :success
  end

  def test_from_path_redirect_to_to_path
    get "/#{@redirection.from_path}"
    assert_equal status, 301
  end
end
