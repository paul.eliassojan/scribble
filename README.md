# Article publishing dashboard

An article management system that includes a dashboard and an end-user interface to view published articles. The dashboard has features like assigning an article to different categories, version histories, custom redirection, publish later, article analytics, and password-protect organization.

# Local Development setup

Step 1: To setup db and install the required gems

```shell
./bin/setup
```

Step 2: To start the rails server

```shell
rails s
```

Step 3: To start webpacker

```shell
./bin/webpack-dev-server
```
